kernelsize = 3;
stride = 1;
padding = 0;
dilate=1;




W=linspace(1,128,128);

output(1,:)=W;
output(2,:)=(output(1,:)-(dilate*(kernelsize-1)+1)+2*padding)/stride+1;
output(3,:)=(output(2,:)-(dilate*(kernelsize-1)+1)+2*padding)/stride+1;
output(4,:)=(output(3,:)-(dilate*(kernelsize-1)+1)+2*padding)/stride+1;
output(5,:)=(output(4,:)-(dilate*(kernelsize-1)+1)+2*padding)/stride+1;
output(6,:)=(output(5,:)-(dilate*(kernelsize-1)+1)+2*padding)/stride+1;
output(7,:)=(output(6,:)-(dilate*(kernelsize-1)+1)+2*padding)/stride+1;
output(8,:)=(output(7,:)-(dilate*(kernelsize-1)+1)+2*padding)/stride+1;

