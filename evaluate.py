""" Evaluate results

KM van Hespen, UMC Utrecht, 2020
"""

##
# LIBRARIES
from __future__ import print_function
from collections import OrderedDict
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np
import os
from scipy.interpolate import interp1d
import scipy.ndimage as ndimage
from scipy.optimize import brentq
from sklearn.metrics import roc_curve, auc, average_precision_score, f1_score, precision_recall_curve

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

def evaluate(labels, scores, metric='roc', saveto=None):
    """Runs the evaluation metric: ROC, AUPRC or F1score"""
    if metric == 'roc':
        return roc(labels, scores, saveto)
    elif metric == 'auprc':
        return auprc(labels, scores)
    elif metric == 'f1_score':
        threshold = 0.20
        scores[scores >= threshold] = 1
        scores[scores < threshold] = 0
        return f1_score(labels, scores)
    else:
        raise NotImplementedError("Check the evaluation metric.")

##
def roc(labels, scores, saveto=None):
    """Compute ROC curve and ROC area for each class"""
    fpr = dict()
    tpr = dict()
    roc_auc = dict()

    labels = labels.cpu()
    scores = scores.cpu()
    np.seterr(divide='ignore', invalid='ignore')
    # True/False Positive Rates.
    fpr, tpr, thresholds = roc_curve(labels, scores)
    prec, rec, _ = precision_recall_curve(labels, scores)
    roc_auc = auc(fpr, tpr)
    prec_auc = auc(rec, prec)
    # Equal Error Rate
    eer = brentq(lambda x: 1. - x - interp1d(fpr, tpr)(x), 0., 1.)
    eer_thresh_loc = interp1d(fpr, np.linspace(0, fpr.shape[0] - 1, fpr.shape[0]))(eer)
    error_threshold = interp1d(np.linspace(0, fpr.shape[0] - 1, fpr.shape[0]), thresholds)(eer_thresh_loc)

    if saveto:
        if not os.path.isdir(saveto):
            os.makedirs(saveto)
        plt.figure()
        lw = 2
        plt.plot(fpr, tpr, color='darkorange', lw=lw, label='(AUC = %0.2f, EER = %0.2f)' % (roc_auc, eer))
        plt.plot([eer], [1-eer], marker='o', markersize=5, color="navy")
        plt.plot([0, 1], [1, 0], color='navy', lw=1, linestyle=':')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic')
        plt.legend(loc="lower right")
        plt.savefig(os.path.join(saveto, "ROC.pdf"))
        plt.close()

        plt.figure()
        lw = 2
        plt.plot(rec, prec, color='darkorange', lw=lw, label='(AUC = %0.2f, EER = %0.2f)' % (prec_auc, eer))
        plt.plot([eer], [1-eer], marker='o', markersize=5, color="navy")
        plt.plot([0, 1], [1, 0], color='navy', lw=1, linestyle=':')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.title('Precision recall curve')
        plt.legend(loc="lower right")
        plt.savefig(os.path.join(saveto, "PRECREC.pdf"))
        plt.close()

    return roc_auc, prec_auc, error_threshold

def auprc(labels, scores):
    "average precision score"
    ap = average_precision_score(labels, scores)
    return ap

def reconstruction_by_erosion(image, struct=np.ones((7, 7, 7, 1)), struct_dilate=np.ones((7, 7, 7, 1))):
    """Reconstruction by erosion
    INPUT:      image: Innput image
                struct: structure used for erosion (np array)
                struct_dilate: structure for subsequent dilation (np array)
    """
    eroded_square = ndimage.binary_erosion(image, structure=struct)
    reconstruction = ndimage.binary_propagation(eroded_square, structure=struct_dilate, mask=image)
    return reconstruction

def imagelevel_evaluation(opt, error, threshold, origin, lesion_original_masks, labeled_original, ncomponents_original):
    """ Projects the anomaly scores back into the original image space, and computes the number of (missed) detections
    INPUT:      opt: options
                error: vector containing all error scores
                threshold: the Z-score threshold above which an anomaly is registered
                origin: vector with the coordinates of all the elements in the error vector
                lesion_original_masks: Original manual infarct segmentation masks
                labeled_original: Labeled version of lesion_original_masks, where each annotation gets a separate label
                ncomponents_original: number of infarcts, should be equal to the number of unique values>0 in labeled_original

    OUTPUT:     String containing all the evaluation metrics, including FP, FN, TP, TN, F1, Sens, Spec, etc.
    """
    np.seterr(divide='ignore', invalid='ignore')
    temp_img = np.zeros((opt.isize, opt.isize, 38) + (np.max(origin[:, 3]) + 1,))
    extent = (opt.patchsize - 1) // (opt.slidingwindowstride * 2)
    for origin_point in range(0, origin.shape[0]):
        temp_img[origin[origin_point, 0] - extent:origin[origin_point, 0] + extent + 1, origin[origin_point, 1] - extent:origin[origin_point, 1] + extent + 1, origin[origin_point, 2], origin[origin_point, 3]] = error[origin_point]
    lesion_mask = temp_img > threshold
    labelstruct = ndimage.morphology.generate_binary_structure(4, 4)
    labelstruct[..., 0] = False
    labelstruct[..., 2] = False

    labeled_guesses, ncomponents = ndimage.measurements.label(lesion_mask, structure=labelstruct)
    uniques = np.unique(labeled_guesses)
    holdvolume = np.zeros((uniques.shape[0] + 1, 1))
    holdvolume = ndimage.measurements.labeled_comprehension(labeled_guesses > 0, labeled_guesses, np.linspace(1, uniques.shape[0] - 1, uniques.shape[0] - 1), np.sum, np.uint32, 0)
    indices = np.argwhere(holdvolume <= (np.power((extent) * 2 + 1, 2)) * 5) + 1
    lesion_mask_cleaned = np.isin(labeled_guesses, np.append(indices, 0), invert=True)
    labeled_guesses_cleaned, ncomponents_cleaned = ndimage.measurements.label(lesion_mask_cleaned, structure=labelstruct)

    correct_guesses = np.unique(labeled_original * lesion_mask)
    guesses_cleaned = np.unique(labeled_original * lesion_mask_cleaned)
    num_correct_guesses = correct_guesses.shape[0] - 1
    numguesses_cleaned = guesses_cleaned.shape[0] - 1
    total_guesses = np.unique(lesion_original_masks*labeled_guesses).shape[0]
    total_guesses_cleaned = np.unique(lesion_original_masks * labeled_guesses_cleaned).shape[0]
    tp = num_correct_guesses
    fn = ncomponents_original - num_correct_guesses
    fp = ncomponents - (total_guesses - 1)
    tp_cleaned = numguesses_cleaned
    fn_cleaned = ncomponents_original - numguesses_cleaned
    fp_cleaned = ncomponents_cleaned - (total_guesses_cleaned - 1)

    return OrderedDict([('True lesions', ncomponents_original), ('estimated lesions', ncomponents), ('TP', tp), ('FP', fp), ('FN', fn), ('sens', tp / (tp + fn)), ('prec', tp / (tp + fp)), ('F1', 2 * tp / (tp + fp) * tp / (tp + fn) / (tp / (tp + fn) + tp / (tp + fp))), ('True lesions', ncomponents_original), ('estimated cleaned lesions', ncomponents_cleaned), ('TP cleaned', tp_cleaned), ('FP cleaned', fp_cleaned), ('FN cleaned', fn_cleaned), ('sens cleaned', tp_cleaned / (tp_cleaned + fn_cleaned + 0.0001)), \
                        ('prec cleaned', tp_cleaned / (tp_cleaned + fp_cleaned + 0.0001)), ('F1 cleaned', 2 * tp_cleaned / (tp_cleaned + fp_cleaned + 0.0001) * tp_cleaned / (tp_cleaned + fn_cleaned + 0.0001)/((0.0001 + tp_cleaned) / (tp_cleaned + fn_cleaned + 0.0001) + tp_cleaned / (tp_cleaned + fp_cleaned + 0.0001)))])
