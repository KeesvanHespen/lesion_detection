"""
App to assess the origin of false positive detections of the anomaly detection algorithm.

KM van Hespen, UMC Utrecht, 2020
"""

from mevis import *
import numpy as np
import os
import pickle
import scipy.ndimage as ndimage

def load():
  # Opens the FPanalysis.npy file, and loads the first patient image.
  global uniquesorigin, ptlist, FPlistorigin, commentlist, autocontinue, uniques
  autocontinue = False
  
  if not len(ctx.field("resultsname").value)<1:
    
    ctx.field("itkImageFileReader.unresolvedFileName").setValue(ctx.field("resultsname").value)
    T1W = ctx.field("itkImageFileReader.output0").image()
    T1W = T1W.getTile( (0, 0, 0, 0, 0), (T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent) )
    uniques = np.unique(T1W)
    uniques = uniques[1:]
    ctx.field("numlesions").setValue(uniques.shape[0])
    if os.path.isfile(ctx.field("resultsname").value.replace(ctx.field("FileInformation2.filename").value, 'FPanalysis.npy')):
      FPlistorigin = np.load(ctx.field("resultsname").value.replace(ctx.field("FileInformation2.filename").value, 'FPanalysis.npy'))
      last_analysed = np.where(FPlistorigin != -1)[0]
    else:
      FPlistorigin = -np.ones(uniques.shape, dtype=object)
      last_analysed = np.array([])
    uniquesorigin = np.zeros(uniques.shape, dtype=int)     
    commentlist = ['']
    ctx.field("FieldListener.shouldAutoUpdate").setValue(False)
    for pt in range(0,T1W.shape[1] + 1):
      ctx.field("SubImage.c").setValue(pt)
      curlesionimg = ctx.field("SubImage.output0").image()
      curlesionimg = curlesionimg.getTile( (0, 0, 0, 0, 0), (curlesionimg.UseImageExtent, curlesionimg.UseImageExtent, curlesionimg.UseImageExtent, curlesionimg.UseImageExtent, curlesionimg.UseImageExtent) )
    
      uniquescurlesion = np.unique(curlesionimg)
      uniquesorigin[np.isin(uniques, uniquescurlesion)] = pt
    
    ptlist = np.load(ctx.field("resultsname").value.replace(ctx.field("FileInformation2.filename").value, 'patientlist.npy'))
    try:
      ctx.field("FileInformation1.path").setValue(ptlist[0].decode('UTF-8'))
      ptlist = [st.decode('UTF-8') for st in ptlist]
    except:
      ctx.field("FileInformation1.path").setValue(ptlist[0]) 
    if ctx.field("DeployedforRelease").value:
      ptlist = [st.replace(ptlist[0][:-len(ctx.field("FileInformation1.filename").value)],ctx.field("curdir").value + '/SMART/') for st in ptlist]
    ctx.field("StringUtils.string1").setValue(ptlist[uniquesorigin[ctx.field("curlesion").value - 1]])

    ctx.field("curlesion").setValue(1)
    if last_analysed.shape[0] > 0:
      ctx.field("curlesion").setValue(np.max(last_analysed) + 1)
    
    ctx.field("IntervalThreshold.threshCenter").setValue(uniques[ctx.field("curlesion").value - 1])
    ctx.field("SubImage.c").setValue(uniquesorigin[ctx.field("curlesion").value - 1])

    ctx.field("displayedptnumber").setValue(uniquesorigin[ctx.field("curlesion").value - 1])
    MLAB.processEvents()
    MLAB.processInventorQueue()
    ctx.field("FieldListener.shouldAutoUpdate").setValue(True)
    ctx.field("RunPythonScript.execute").touch()
    curval = '%i/%i'%(ctx.field("curlesion").value,ctx.field("numlesions").value)
    ctx.field("currentlesionnumber").setValue(curval)
    autocontinue = True

def update():
  # Visualizes the next false positive, and if available sets the already selected category.
  global uniquesorigin, ptlist, FPlistorigin, commentlist, autocontinue, uniques
  if not uniquesorigin[ctx.field("curlesion").value - 1] == ctx.field("displayedptnumber").value:
    ctx.field("StringUtils.string1").setValue(ptlist[uniquesorigin[ctx.field("curlesion").value - 1]])
  ctx.field("displayedptnumber").setValue(uniquesorigin[ctx.field("curlesion").value - 1])
  ctx.field("FieldListener.shouldAutoUpdate").setValue(False)
  ctx.field("SubImage.c").setValue(uniquesorigin[ctx.field("curlesion").value - 1])
  ctx.field("IntervalThreshold.threshCenter").setValue(uniques[ctx.field("curlesion").value - 1])
  
  MLAB.processEvents()
  MLAB.processInventorQueue()
  
  autocontinue = False
  try:
    int(FPlistorigin[ctx.field("curlesion").value - 1])
    if int(FPlistorigin[ctx.field("curlesion").value - 1]) < 0:
      ctx.field("FP").setValue(0)
    else:
      ctx.field("FP").setValue(FPlistorigin[ctx.field("curlesion").value - 1])
    ctx.field("curcomment").setValue('')
  except:
    ctx.field("curcomment").setValue(FPlistorigin[ctx.field("curlesion").value - 1])

    ctx.field("FP").setValue(10) 
  ctx.field("FieldListener.shouldAutoUpdate").setValue(True)
  ctx.field("RunPythonScript.execute").touch()
  np.save(ctx.field("resultsname").value.replace(ctx.field("FileInformation2.filename").value, 'FPanalysis.npy'), FPlistorigin)
  curval = '%i/%i'%(ctx.field("curlesion").value, ctx.field("numlesions").value)
  ctx.field("currentlesionnumber").setValue(curval)
  autocontinue=True
  ctx.field("SoToggle.on").setValue(True)
  
def updateFPlist():  
  # Updates the false positive list, with the current selected category
  global FPlistorigin, commentlist, autocontinue
  try:
    if int(ctx.field("FP").value) < 10:
      FPlistorigin[ctx.field("curlesion").value - 1] = ctx.field("FP").value
      if autocontinue and ctx.field("curlesion").value < FPlistorigin.shape[0]:  
        ctx.field("curlesion").value = ctx.field("curlesion").value + 1
    else:
      FPlistorigin[ctx.field("curlesion").value - 1] = ctx.field("curcomment").value
    
  except:
    a = 1 #except
    

def init(): 
  ctx.field("itkImageFileReader.close").touch()
  ctx.field("itkImageFileReader1.close").touch()
  ctx.field("itkImageFileReader2.close").touch()
  if ctx.field("DeployedforRelease").value:
    load()

def shouldClose():  
  # prevents unwanted closing of the app.  
  global FPlistorigin
  try:
    if int(ctx.field("FP").value)<10:
      FPlistorigin[ctx.field("curlesion").value - 1] = ctx.field("FP").value
    else:
      FPlistorigin[ctx.field("curlesion").value - 1] = ctx.field("curcomment").value
    np.save(ctx.field("resultsname").value.replace(ctx.field("FileInformation2.filename").value, 'FPanalysis.npy'), FPlistorigin)
  except:
    a = 1 #except

  if MLAB.isStandaloneApplication():
    closeAllowed = MLAB.showQuestion("Are you sure you want to quit? Any unsaved data will be lost.", "Quit DampingUI", ['Yes', 'No'], 1) == 0
  ctx.window().setCloseAllowed(closeAllowed)