"""GANomaly
"""
# pylint: disable=C0301,E1101,W0622,C0103,R0902,R0915

##
from collections import OrderedDict

import os
import time

import pickle
import numpy as np
import h5py
import scipy.ndimage as ndimage
import scipy.stats as stat

from tqdm import tqdm

import torch.optim as optim
import torch.cuda
import torch.nn as nn
import torch.utils.data
import torchvision.utils as vutils

import data
from evaluate import evaluate, imagelevel_evaluation, reconstruction_by_erosion
from networks import NetG, NetD, weights_init
from loss import l2_loss, l2_loss_np
from visualizer import Visualizer

class BaseModel():
    """ Base Model for ganomaly
    """
    def __init__(self, opt, dataloader):
        ##
        # Seed for deterministic behavior
        self.seed(opt.manualseed)

        # Initalize variables.
        self.opt = opt
        self.visualizer = Visualizer(opt)
        self.dataloader = dataloader
        self.trn_dir = os.path.join(self.opt.outf, self.opt.dataname, 'train')
        self.tst_dir = os.path.join(self.opt.outf, self.opt.dataname, 'test')
        self.device = torch.device("cuda:0" if self.opt.device != 'cpu' else "cpu")


    ##
    def set_input(self, input: torch.Tensor):
        """ Set input and ground truth

        Args:
            input (FloatTensor): Input data for batch i.
        """
        with torch.no_grad():
            self.input.resize_(input[0].size()).copy_(input[0])
            self.gt.resize_(input[1].size()).copy_(input[1])
            self.label.resize_(input[1].size())

            # Copy the first batch as the fixed input.
            if self.total_steps == self.opt.batchsize:
                self.fixed_input.resize_(input[0].size()).copy_(input[0])

    ##
    def seed(self, seed_value):
        """ Seed

        Arguments:
            seed_value {int} -- [description]
        """
        # Check if seed is default value
        if seed_value == -1:
            return

        # Otherwise seed all functionality
        import random
        random.seed(seed_value)
        torch.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value)
        np.random.seed(seed_value)
        torch.backends.cudnn.deterministic = True

    ##
    def get_errors(self):
        """ Get netD and netG errors.

        Returns:
            [OrderedDict]: Dictionary containing errors.
        """

        errors = OrderedDict([
            ('err_d', self.err_d.item()),
            ('err_d_real', self.err_d_real.item()),
            ('err_d_fake', self.err_d_fake.item()),
            ('err_g', self.err_g.item()),
            ('err_g_adv', self.err_g_adv.item()),
            ('err_g_con', self.err_g_con.item()),
            ('err_g_enc', self.err_g_enc.item())])
        errors_npy = np.array([self.err_d.item(),
             self.err_d_real.item(),
             self.err_d_fake.item(),
             self.err_g.item(),
             self.err_g_adv.item(),
             self.err_g_con.item(),
             self.err_g_enc.item()])

        return errors, errors_npy
    def set_errors(self, errors_npy):
        """ Set netD and netG errors.

        Returns:
            [OrderedDict]: Dictionary containing errors.
        """

        errors = OrderedDict([
            ('err_d', errors_npy[0]),
            ('err_d_real', errors_npy[1]),
            ('err_d_fake', errors_npy[2]),
            ('err_g', errors_npy[3]),
            ('err_g_adv', errors_npy[4]),
            ('err_g_con', errors_npy[5]),
            ('err_g_enc', errors_npy[6])])


        return errors

    def set_validationerrors(self, errors_npy):
        """ Set netD and netG errors.

        Returns:
            [OrderedDict]: Dictionary containing errors.
        """

        errors = OrderedDict([
            ('errval_d', errors_npy[0]),
            ('errval_d_real', errors_npy[1]),
            ('errval_d_fake', errors_npy[2]),
            ('errval_g', errors_npy[3]),
            ('errval_g_adv', errors_npy[4]),
            ('errval_g_con', errors_npy[5]),
            ('errval_g_enc', errors_npy[6])])


        return errors
    ##
    def get_current_images(self):
        """ Returns current images.

        Returns:
            [reals, fakes, fixed]
        """

        reals = self.input.data
        fakes = self.fake.data
        fixed = self.netg(self.fixed_input)[0].data

        return reals, fakes, fixed

    ##
    def save_weights(self, epoch):
        """Save netG and netD weights for the current epoch.

        Args:
            epoch ([int]): Current epoch number.
        """

        weight_dir = os.path.join(self.opt.outf, self.opt.dataname, self.opt.experimentname, 'train', 'weights')
        if not os.path.exists(weight_dir): os.makedirs(weight_dir)

        torch.save({'epoch': epoch + 1, 'state_dict': self.netg.state_dict()},
                   '%s/netG.pth' % (weight_dir))
        torch.save({'epoch': epoch + 1, 'state_dict': self.netd.state_dict()},
                   '%s/netD.pth' % (weight_dir))

    ##
    def train_one_epoch(self):
        """ Train the model for one epoch.
        """
        self.netd.train()
        self.netg.train()
        epoch_iter = 0
        error_hold = np.zeros((7,))
        if self.opt.augment:
            augmentations = np.random.randint(0, 2, (len(self.dataloader['train']), 2))
        for curdata in tqdm(self.dataloader['train'], leave=False, disable=True, total=len(self.dataloader['train'])):
            #augmentation
            if self.opt.augment and augmentations[epoch_iter // self.opt.batchsize, 0]:
                curdata[0] = curdata[0].flip([-2])
            if self.opt.augment and augmentations[epoch_iter // self.opt.batchsize, 1]:
                curdata[0] = curdata[0].flip([-1])

            self.total_steps += self.opt.batchsize
            epoch_iter += self.opt.batchsize
            self.set_input(curdata)
            self.optimize_params()
            _, errors_npy = self.get_errors()
            error_hold = error_hold + errors_npy
            if self.total_steps % self.opt.print_freq == 0:
                errors, _ = self.get_errors()
                if self.opt.display:
                    counter_ratio = float(epoch_iter) / len(self.dataloader['train'].dataset)
                    self.visualizer.plot_current_errors(self.epoch, counter_ratio, errors)

            if self.total_steps % self.opt.save_image_freq == 0:
                reals, fakes, fixed = self.get_current_images()
                self.visualizer.save_current_images(self.epoch, reals, fakes, fixed)
                if self.opt.display:
                    self.visualizer.display_current_images(reals, fakes, fixed)

        print(">> Training model %s. Epoch %d/%d" % (self.name, self.epoch + 1, self.opt.niter_max))


        error_hold = error_hold / (epoch_iter // self.opt.batchsize)
        print_errors = self.set_errors(error_hold)
        self.visualizer.print_current_errors(self.epoch, print_errors)
        return error_hold

    ##
    def train_and_test(self):
        """ Train the model
        """

        ##
        # TRAIN
        print(self.netg)
        print(self.netd)
        self.total_steps = 0
        best_auc = 100000
        low_update = 0
        lesion_original_masks = pickle.load(open(os.path.join(self.opt.saveroot, 'val_masks_' + self.opt.dataname), "rb"))
        lesion_original_masks = lesion_original_masks[..., 1, :] > 50
        lesion_original_masks = reconstruction_by_erosion(lesion_original_masks, struct=np.ones((3, 3, 1, 1)), struct_dilate=np.ones((3, 3, 3, 1)))
        labelstruct = ndimage.morphology.generate_binary_structure(4, 4)
        labelstruct[..., 0] = False
        labelstruct[..., 2] = False
        labeled_original, ncomponents_original = ndimage.measurements.label(lesion_original_masks, structure=labelstruct)

        labeled_original = labeled_original * lesion_original_masks
        self.times_epoch = []
        # Train for niter epochs.
        print(">> Training model %s." % self.name)
        best_epoch = 0
        # save options dictionary

        np.save(os.path.join(self.opt.outf, self.opt.dataname, self.opt.experimentname, 'options_dict'), self.opt)
        for self.epoch in range(self.opt.iter, self.opt.niter_max):
            # Train for one epoch
            time_i = time.time()
            errors = self.train_one_epoch()
            res, _, _, _, _, _ = self.test('validation')
            errorsvalidation = self.validate()
            _, _, _, _, lat_i, lat_o = self.test('train')
            train_nonzeros = int(self.opt.nrofpatches[0]) // self.opt.batchsize * self.opt.batchsize
            lat_i = lat_i[0:train_nonzeros, ...]
            lat_o = lat_o[0:train_nonzeros, ...]
            encoding_error_train = l2_loss_np(lat_i, lat_o, False)
            y = np.power((lat_i - lat_o), 2)
            medx = np.median(encoding_error_train)
            madx = stat.median_absolute_deviation(encoding_error_train)


            _, _, error_all, label_all, lat_i_val, lat_o_val = self.test(phase='validation_all', output_ROC=False)

            z = np.power((lat_i_val - lat_o_val), 2)
            lat_i_val = []
            lat_o_val = []
            label_all = label_all.cpu().numpy()
            rZscore = np.abs(error_all.cpu().numpy() - medx) / madx
            self.visualizer.print_current_performance(imagelevel_evaluation(self.opt, rZscore, 3, self.dataloader['validation_all'].dataset.origin, lesion_original_masks, labeled_original, ncomponents_original), 3)

            medx = np.median(y, axis=0)
            madx = stat.median_absolute_deviation(y, axis=0)
            rZscore3 = np.mean(np.abs(z - medx) / madx, axis=1)

            rZscore2 = np.median(np.abs(z - medx) / madx, axis=1)
            self.visualizer.print_current_performance(imagelevel_evaluation(self.opt, rZscore2, 3, self.dataloader['validation_all'].dataset.origin, lesion_original_masks, labeled_original, ncomponents_original), 3.1)

            if (best_auc-errorsvalidation[3]) / best_auc < 2e-3:
                low_update = low_update + 1
            else:
                low_update = 0

            if errorsvalidation[3] < best_auc:
                best_epoch = self.epoch
                best_auc = errorsvalidation[3]
                best_medx = medx
                best_madx = madx
                if not os.path.exists(os.path.join(self.opt.outf, self.opt.dataname, self.opt.experimentname, 'validation')): os.makedirs(os.path.join(self.opt.outf, self.opt.dataname, self.opt.experimentname, 'validation'))
                self.save_weights(self.epoch)
                error_label = np.concatenate((error_all.cpu().numpy()[..., None], label_all[..., None], rZscore[..., None], rZscore3[..., None], rZscore2[..., None]), 1)
                np.save(os.path.join(self.opt.outf, self.opt.dataname, self.opt.experimentname, 'validation', 'validation_all_results.npy'), error_label)
                np.save(os.path.join(self.opt.outf, self.opt.dataname, self.opt.experimentname, 'validation', 'validation_feature_zscores.npy'), np.abs(z - medx) / madx)
            time_o = time.time()
            self.times_epoch = (time_o - time_i)
            print('   Epoch train time (s): %i.' % self.times_epoch)
            self.visualizer.print_current_performance(res, best_auc)

            if self.epoch > (best_epoch + self.opt.niter_history) or low_update > (self.opt.niter_history // 2):
                print('   Stopping early. Validation accuracy did not improve within %i epochs' % self.opt.niter_history)
                break;

        print(">> Training model %s.[Done]" % self.name)
        print(">> Testing model %s." % self.name)
        #check if medx in local variables. If not then deduce from training data and best weights
        if not 'best_medx' in locals():
            temp_loadweights = self.opt.load_weights
            self.opt.load_weights = True
            _, _, _, _, lat_i, lat_o = self.test('train')
            self.opt.load_weights = temp_loadweights
            encoding_error_train = l2_loss_np(lat_i, lat_o, False)
            y = np.power((lat_i - lat_o), 2)
            best_medx = np.median(y, axis=0)
            best_madx = stat.median_absolute_deviation(y, axis=0)
        medx_mean = np.median(encoding_error_train)
        madx_mean = stat.median_absolute_deviation(encoding_error_train)
        lat_i = []
        lat_o = []
        encoding_error_train = []

        lesion_original_masks = pickle.load(open(os.path.join(self.opt.saveroot, 'test_masks_' + self.opt.dataname), "rb"))
        lesion_original_masks = lesion_original_masks[..., 1, :] > 0
        lesion_original_masks = reconstruction_by_erosion(lesion_original_masks, struct=np.ones((3, 3, 1, 1)), struct_dilate=np.ones((3, 3, 3, 1)))
        if self.opt.hdf5testsampling == True:
            with h5py.File(os.path.join(self.opt.saveroot, 'testdata_' + self.opt.dataname + '.hdf5'), 'r') as f:
                nr_of_chunks = len(f.keys()) // 5
                nr_of_measurements = np.zeros((nr_of_chunks,), dtype=int)
                for chunk in range(0, nr_of_chunks):
                    nr_of_measurements[chunk] = f['target_' + str(chunk)][()].shape[0]
            prevchunksize = 0

            error_label = np.zeros((np.sum(nr_of_measurements), 4), dtype=np.float32)
            error_all = np.zeros((np.sum(nr_of_measurements), best_medx.shape[0]), dtype=np.float32)
            nr_of_measurements = np.cumsum(nr_of_measurements)
            for chunk in range(0, nr_of_chunks):
                print('   Chunk (%i/%i):'%(chunk + 1, nr_of_chunks))
                self.dataloader, _ = data.load_testdata(self.opt, chunk)
                chunksize = np.unique(self.dataloader['test'].dataset.origin[:, -1]).shape[0]
                _, _, error, label, lat_i_test, lat_o_test = self.test(output_fakes=False, output_ROC=True)

                labeled_original, ncomponents_original = ndimage.measurements.label(ndimage.binary_dilation(lesion_original_masks[..., prevchunksize:chunksize + prevchunksize], structure=labelstruct))
                labeled_original = labeled_original * lesion_original_masks[..., prevchunksize:chunksize + prevchunksize]

                error = error.cpu().numpy()
                label = label.cpu().numpy()
                z = np.power((lat_i_test - lat_o_test), 2)
                lat_i_test = []
                lat_o_test = []
                rZscore_mean = np.abs(error - medx_mean) / madx_mean
                rZscore = np.median(np.abs(z - best_medx) / best_madx, axis=1)
                self.visualizer.print_current_performance(imagelevel_evaluation(self.opt, rZscore_mean, 3, self.dataloader['test'].dataset.origin, lesion_original_masks[..., prevchunksize:chunksize + prevchunksize], labeled_original, ncomponents_original), chunk +1)

                self.visualizer.print_current_performance(imagelevel_evaluation(self.opt, rZscore, 3, self.dataloader['test'].dataset.origin, lesion_original_masks[..., prevchunksize:chunksize + prevchunksize], labeled_original, ncomponents_original), chunk + 1)
                prevchunksize = prevchunksize + chunksize
                if chunk == 0:
                    error_label[0:nr_of_measurements[chunk]] = np.concatenate((error[..., None], label[..., None], rZscore_mean[..., None], rZscore[..., None]), 1)
                    error_all[0:nr_of_measurements[chunk]] = np.abs(z - best_medx) / best_madx
                elif chunk+1 == nr_of_chunks:
                    error_label[nr_of_measurements[chunk-1]:] = np.concatenate((error[..., None], label[..., None], rZscore_mean[..., None], rZscore[..., None]), 1)
                    error_all[nr_of_measurements[chunk-1]:] = np.abs(z - best_medx) / best_madx
                else:
                    error_label[nr_of_measurements[chunk-1]:nr_of_measurements[chunk]] = np.concatenate((error[..., None], label[..., None], rZscore_mean[..., None], rZscore[..., None]), 1)
                    error_all[nr_of_measurements[chunk-1]:nr_of_measurements[chunk]] = np.abs(z-best_medx)/best_madx

            np.save(os.path.join(self.opt.outf, self.opt.dataname, self.opt.experimentname, 'test', 'test_feature_zscores.npy'), error_all)

            np.save(os.path.join(self.opt.outf, self.opt.dataname, self.opt.experimentname, 'test', 'test_results.npy'), error_label)

        else:
            self.dataloader, _ = data.load_testdata(self.opt)
            res, _, error, label, lat_i_test, lat_o_test = self.test(output_fakes=False, output_ROC=True)
            self.visualizer.print_current_performance(res, -1)
            z = np.power((lat_i_test - lat_o_test), 2)
            error = error.cpu().numpy()
            label = label.cpu().numpy()
            error_label = np.concatenate((error[..., None], label[..., None]), 1)
            np.save(os.path.join(self.opt.outf, self.opt.dataname, self.opt.experimentname, 'test', 'test_results.npy'), error_label)
            np.save(os.path.join(self.opt.outf, self.opt.dataname, self.opt.experimentname, 'test', 'test_feature_zscores.npy'), np.abs(z - best_medx) / best_madx)



        torch.cuda.empty_cache()
        print(">> Testing model %s.[Done]" % self.name)
    ##


    def validate(self, phase='test', output_fakes=False, output_ROC=False):
        epoch_iter = 0
        with torch.no_grad():
            self.netg.eval()
            self.netd.eval()
            error_hold = np.zeros((7,))
            for curdata in tqdm(self.dataloader['trainvalidation'], leave=False, disable=True, total=len(self.dataloader['trainvalidation'])):
                epoch_iter += self.opt.batchsize
                self.set_input(curdata)
                _, errors_npy = self.get_errors()
                error_hold = error_hold + errors_npy

            error_hold = error_hold / (epoch_iter // self.opt.batchsize)
            print_errors = self.set_validationerrors(error_hold)
            self.visualizer.print_current_errors(-(self.epoch + 2), print_errors)
        return error_hold

    def test(self, phase='test', output_fakes=False, output_ROC=False):
        """ Test GANomaly model.

        Args:
            dataloader ([type]): Dataloader for the test set

        Raises:
            IOError: Model weights not found.
        """
        with torch.no_grad():

            # Load the weights of netg and netd.
            if phase == 'test' or self.opt.load_weights:

                path = os.path.join(self.opt.saveroot, self.opt.dataname, self.opt.experimentname, 'train', 'weights', 'netG.pth')
                pretrained_dict = torch.load(path)['state_dict']

                try:
                    self.netg.load_state_dict(pretrained_dict)
                except IOError:
                    raise IOError("netG weights not found")

                path = os.path.join(self.opt.saveroot, self.opt.dataname, self.opt.experimentname, 'train', 'weights', 'netD.pth')
                pretrained_dict = torch.load(path)['state_dict']

                try:
                    self.netd.load_state_dict(pretrained_dict)
                except IOError:
                    raise IOError("netD weights not found")
                print('   Loaded weights.')
            self.netg.eval()
            self.netd.eval()

            self.gt_images = torch.zeros(size=tuple(np.concatenate((np.array(self.dataloader[phase].dataset.data.shape), np.array([2])))), dtype=torch.float32, device='cpu')
            # Create big error tensor for the test set.
            self.an_scores = torch.zeros(size=(len(self.dataloader[phase].dataset),), dtype=torch.float32, device=self.device)
            self.gt_labels = torch.zeros(size=(len(self.dataloader[phase].dataset),), dtype=torch.long, device=self.device)
            latent_i = np.zeros((len(self.dataloader[phase].dataset), self.opt.nz), dtype='float32')
            latent_o = np.zeros((len(self.dataloader[phase].dataset), self.opt.nz), dtype='float32')

            # print("   Testing model %s." % self.name)
            self.times = []
            epoch_iter = 0
            for i, curdata in enumerate(self.dataloader[phase], 0):
                epoch_iter += self.opt.batchsize
                time_i = time.time()
                self.set_input(curdata)
                self.fake, cur_latent_i, cur_latent_o = self.netg(self.input)
                if output_fakes == True:
                    self.gt_images[i * self.opt.batchsize:(i + 1) * self.opt.batchsize, ..., 0] = self.fake.cpu()
                    self.gt_images[i * self.opt.batchsize:(i + 1) * self.opt.batchsize, ..., 1] = self.input.cpu()
                error = torch.mean(torch.pow((cur_latent_i - cur_latent_o), 2), dim=1)
                time_o = time.time()

                self.an_scores[i * self.opt.batchsize : i * self.opt.batchsize + error.size(0)] = error.reshape(error.size(0))
                self.gt_labels[i  *self.opt.batchsize : i * self.opt.batchsize + error.size(0)] = self.gt.reshape(error.size(0))
                latent_i[i * self.opt.batchsize : i * self.opt.batchsize + error.size(0), :] = cur_latent_i.reshape(error.size(0), self.opt.nz).cpu().numpy()
                latent_o[i * self.opt.batchsize : i * self.opt.batchsize + error.size(0), :] = cur_latent_o.reshape(error.size(0), self.opt.nz).cpu().numpy()

                self.times.append(time_o - time_i)

                # Save test images.
                if self.opt.save_test_images:
                    dst = os.path.join(self.opt.outf, self.opt.dataname, self.opt.experimentname, phase, 'images')
                    if not os.path.isdir(dst):
                        os.makedirs(dst)
                    real, fake, _ = self.get_current_images()
                    real = real.view(-1, real.shape[2], real.shape[3]).unsqueeze(1).repeat(1, 3, 1, 1)
                    fake = fake.view(-1, fake.shape[2], fake.shape[3]).unsqueeze(1).repeat(1, 3, 1, 1)
                    vutils.save_image(real, '%s/real_%03d.eps' % (dst, i + 1), normalize=True)
                    vutils.save_image(fake, '%s/fake_%03d.eps' % (dst, i + 1), normalize=True)

            # Measure inference time.
            self.times = np.array(self.times)
            self.times = np.mean(self.times[:100] * 1000)

            # Scale error vector between [0, 1]
            # self.an_scores = (self.an_scores - torch.min(self.an_scores)) / (torch.max(self.an_scores) - torch.min(self.an_scores))
            # auc, eer = roc(self.gt_labels, self.an_scores)
            if output_ROC:
                dst = os.path.join(self.opt.outf, self.opt.dataname, self.opt.experimentname, phase, 'images')
                roc_auc, prec_auc, eer = evaluate(self.gt_labels, self.an_scores, metric=self.opt.metric, saveto=dst)
            else:
                roc_auc, prec_auc, eer = evaluate(self.gt_labels, self.an_scores, metric=self.opt.metric, saveto=None)
            performance = OrderedDict([('Avg Run Time (ms/batch)', self.times), ('EER', eer), ('AUC', roc_auc), ('PRAUC', prec_auc)])

            if self.opt.display_id > 0:
                counter_ratio = float(epoch_iter) / len(self.dataloader[phase].dataset)
                self.visualizer.plot_performance(self.epoch, counter_ratio, performance)
            return performance, self.gt_images, self.an_scores, self.gt_labels, latent_i, latent_o

##
class Ganomaly(BaseModel):
    """GANomaly Class
    """

    @property
    def name(self): return 'Ganomaly'

    def __init__(self, opt, dataloader):
        super().__init__(opt, dataloader)        

        # -- Misc attributes
        self.epoch = 0
        self.times = []
        self.total_steps = 0

        ##
        # Create and initialize networks.
        self.netg = NetG(self.opt).to(self.device)
        self.netd = NetD(self.opt).to(self.device)
        self.netg.apply(weights_init)
        self.netd.apply(weights_init)

        ##
        if self.opt.resume != '':
            print("\nLoading pre-trained networks.")
            self.opt.iter = torch.load(os.path.join(self.opt.resume, 'netG.pth'))['epoch']
            self.netg.load_state_dict(torch.load(os.path.join(self.opt.resume, 'netG.pth'))['state_dict'])
            self.netd.load_state_dict(torch.load(os.path.join(self.opt.resume, 'netD.pth'))['state_dict'])
            print("\tDone.\n")

        self.l_adv = l2_loss
        self.l_con = nn.L1Loss(reduce='none')
        self.l_enc = l2_loss
        self.l_bce = nn.BCELoss()

        ##
        # Initialize input tensors.
        self.input = torch.empty(size=(self.opt.batchsize, 3, self.opt.patchsize, self.opt.patchsize), dtype=torch.float32, device=self.device)
        self.label = torch.empty(size=(self.opt.batchsize,), dtype=torch.float32, device=self.device)
        self.gt = torch.empty(size=(opt.batchsize,), dtype=torch.long, device=self.device)
        self.fixed_input = torch.empty(size=(self.opt.batchsize, 3, self.opt.patchsize, self.opt.patchsize), dtype=torch.float32, device=self.device)
        self.real_label = torch.ones(size=(self.opt.batchsize,), dtype=torch.float32, device=self.device)
        self.fake_label = torch.zeros(size=(self.opt.batchsize,), dtype=torch.float32, device=self.device)
        ##
        # Setup optimizer
        if self.opt.isTrain:
            self.netg.train()
            self.netd.train()
            self.optimizer_d = optim.Adam(self.netd.parameters(), lr=self.opt.lr, betas=(self.opt.beta1, 0.999))
            self.optimizer_g = optim.Adam(self.netg.parameters(), lr=self.opt.lr, betas=(self.opt.beta1, 0.999))

    ##
    def forward_g(self):
        """ Forward propagate through netG
        """
        self.fake, self.latent_i, self.latent_o = self.netg(self.input)

    ##
    def forward_d(self):
        """ Forward propagate through netD
        """
        self.pred_real, self.feat_real = self.netd(self.input)
        self.pred_fake, self.feat_fake = self.netd(self.fake.detach())

    ##
    def backward_g(self):
        """ Backpropagate through netG
        """
        self.err_g_adv = self.l_adv(self.netd(self.input)[1], self.netd(self.fake)[1])
        #self.recon_weights = (self.input>0.01).type(torch.float)
        self.err_g_con = (self.l_con(self.fake, self.input).mean())
        self.err_g_enc = self.l_enc(self.latent_o, self.latent_i)
        self.err_g = self.err_g_adv * self.opt.w_adv + \
                     self.err_g_con * self.opt.w_con + \
                     self.err_g_enc * self.opt.w_enc
        self.err_g.backward(retain_graph=True)

    ##
    def backward_d(self):
        """ Backpropagate through netD
        """
        # Real - Fake Loss
        if self.opt.softlabel:
            self.real_label.uniform_(0.8, 1)
            self.fake_label.uniform_(0, 0.2)
        self.err_d_real = self.l_bce(self.pred_real, self.real_label)
        self.err_d_fake = self.l_bce(self.pred_fake, self.fake_label)

        # NetD Loss & Backward-Pass
        self.err_d = (self.err_d_real + self.err_d_fake) * 0.5
        self.err_d.backward()

    ##
    def reinit_d(self):
        """ Re-initialize the weights of netD
        """
        self.netd.apply(weights_init)
        print('   Reloading net d')

    def optimize_params(self):
        """ Forwardpass, Loss Computation and Backwardpass.
        """
        # Forward-pass
        self.forward_g()
        self.forward_d()

        # Backward-pass
        # netg
        self.optimizer_g.zero_grad()
        self.backward_g()
        self.optimizer_g.step()

        # netd
        self.optimizer_d.zero_grad()
        self.backward_d()
        self.optimizer_d.step()
        if self.err_d_real.item() < 1e-5 or self.err_d_fake.item() < 1e-5: self.reinit_d()
