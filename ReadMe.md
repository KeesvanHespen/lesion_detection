Code used for the detection of chronic brain infarcts. Neural network and scripts adapted from the Ganomaly network by Akcay et al. (arXiv:1805.06725)

Running Main.py will start the training of the anomaly detection neural network. All files needed for this execution are included in the current directory (data, evaluate, loss, model, networks, options, visualizer).

The two viewer scripts in Visualization scripts are used for visualizing anomaly score heat maps and performing nearest neighbor visualization.

Supporting scripts and functions contain some editing scripts to change the brain masks.

FalsePositiveChecker can be used to categorize the falsepositives, as has been used by JWD in the false positive analysis in the paper.

PlotNeuralNet used from Haris Iqbal(https://github.com/HarisIqbal88/PlotNeuralNet)

KM van Hespen, UMC Utrecht, 2020