"""
LOAD DATA from file.
"""

# pylint: disable = C0301, E1101, W0622, C0103, R0902, R0915

##
import glob
import os


import numpy as np
import pickle
import h5py
import nibabel
import random
import scipy
import scipy.ndimage
import torch
from tqdm import tqdm

##
def load_data(opt):
    """ Load Data

    Args:
        opt ([type]): Argument Parser

    Raises:
        IOError: Cannot Load Dataset

    Returns:
        [type]: dataloader
    """

    ##
    # LOAD DATA SET
    lesionlist = []
    nolesionlist = []
    patientlist = glob.glob(opt.dataroot+'/*')
    patientlist_temp = [item for item in patientlist if item[-4:] not in opt.skiplist]
    patientlist = patientlist_temp
    for patient in patientlist:
        if len(glob.glob(patient + '/Probmap7*')) > 0:
            lesionlist.append(patient)
        else:
            nolesionlist.append(patient)
    testpreps = len(lesionlist) - np.int(np.round(len(lesionlist) / (1 + opt.test_val_ratio)))
    if testpreps > 50 and opt.nrofpatches[2] == np.inf:
        opt.hdf5testsampling = True
        test_file_extension = '.hdf5'
    else:
        test_file_extension = ''
        opt.hdf5testsampling = False

    if not os.path.isfile(os.path.join(opt.saveroot, 'trainvaldata_' + opt.dataname)):
        for path, index in tqdm(zip(nolesionlist, range(len(nolesionlist))), total=len(nolesionlist), leave=True, desc="Loading train data masks"):
            brainmask = (nibabel.load(path + '/brainmask_removedbg.nii.gz')).get_fdata() > 0.5
            ventmask = ((nibabel.load(path + '/Ventricle_mask.nii.gz')).get_fdata() > 0.5).astype('uint8')
            brainmaskedges = brainmask + 0.0 - scipy.ndimage.morphology.binary_erosion(brainmask + 0.0)
            if index == 0:
                dataset_nolesion_masks = np.zeros(np.append(np.array(brainmask.shape), np.array([2, len(nolesionlist)])), dtype='uint8')

            dataset_nolesion_masks[..., 0, index] = brainmask + ventmask + brainmaskedges

        trainmasks = dataset_nolesion_masks[..., :]

        #trainlist = (np.asarray(nolesionlist)[:])
        shuffle_arraytrain = np.array(range(0, len(nolesionlist)))
        np.random.seed(10)
        np.random.shuffle(shuffle_arraytrain)
        trainsubsetmasks = trainmasks[..., shuffle_arraytrain[0:-10]]
        traintrainlist = (np.asarray(nolesionlist)[shuffle_arraytrain[0:-10]])
        trainvallist = (np.asarray(nolesionlist)[shuffle_arraytrain[-10:]])
        trainvalsubsetmasks = trainmasks[..., shuffle_arraytrain[-10:]]
        dataset_nolesion_masks = []
        nolesionlist = []
    if not os.path.isfile(os.path.join(opt.saveroot, 'trainvaldata_' + opt.dataname)):
        traindata, traintarget, traintargetsegmentation, trainorigin = datasetgeneration(trainvallist, trainvalsubsetmasks, np.array([opt.patchsize, opt.patchsize]), opt, trainortest='train', nr_of_patches=np.inf)#opt.nrofpatches[0]//10)
        trainval = {'data': traindata, 'target':traintarget, 'targetsegmentation': traintargetsegmentation, 'origin':trainorigin, 'fullpatientlist':trainvallist, 'opts':opt}
        with open(os.path.join(opt.saveroot, 'trainvaldata_'+opt.dataname), "wb") as f:
            pickle.dump(trainval, f, protocol=4)
        with open(os.path.join(opt.saveroot, 'trainval_masks_'+opt.dataname), "wb") as f:
                    pickle.dump(trainvalsubsetmasks, f, protocol=4)
                    
    if not os.path.isfile(os.path.join(opt.saveroot, 'traindata_'+opt.dataname)):
        traindata, traintarget, traintargetsegmentation, trainorigin = datasetgeneration(traintrainlist, trainsubsetmasks, np.array([opt.patchsize, opt.patchsize]), opt, trainortest='train', nr_of_patches=opt.nrofpatches[0])
        train = {'data': traindata, 'target':traintarget, 'targetsegmentation': traintargetsegmentation, 'origin':trainorigin, 'fullpatientlist':traintrainlist, 'opts':opt}
        with open(os.path.join(opt.saveroot, 'traindata_' + opt.dataname), "wb") as f:
            pickle.dump(train, f, protocol=4)

    else:
        train = pickle.load(open(os.path.join(opt.saveroot, 'traindata_' + opt.dataname), "rb"))
        trainval = pickle.load(open(os.path.join(opt.saveroot, 'trainvaldata_' + opt.dataname), "rb"))
        if 'opts' in train:
            opt.patchsize = train['opts'].patchsize
            opt.proportion = train['opts'].proportion
            opt.test_val_ratio = train['opts'].test_val_ratio


    if not os.path.isfile(os.path.join(opt.saveroot, 'valdata_'+opt.dataname)) or not os.path.isfile(os.path.join(opt.saveroot, 'testdata_' + opt.dataname + test_file_extension)):
        for path, index in tqdm(zip(lesionlist, range(len(lesionlist))), total=len(lesionlist), leave=True, desc="Loading validation/test data masks"):
            brainmask = (nibabel.load(path + '/brainmask_removedbg.nii.gz')).get_fdata() > 0.5
            probmap = (nibabel.load(path + '/Probmap7_infarct.nii.gz')).get_fdata()

            if index == 0:
                dataset_lesion_masks = np.zeros(np.append(np.array(brainmask.shape), np.array([2, len(lesionlist)])), dtype='uint8')

            dataset_lesion_masks[..., 0, index] = brainmask
            dataset_lesion_masks[..., 1, index] = probmap

        testpreps = np.int(np.round(len(lesionlist)/(1+ opt.test_val_ratio)))
        shuffle_array = np.array(range(0, len(lesionlist)))
        np.random.seed(10)
        np.random.shuffle(shuffle_array)
        valsubsetmasks = dataset_lesion_masks[..., shuffle_array[0:testpreps]]
        with open(os.path.join(opt.saveroot, 'val_masks_'+opt.dataname), "wb") as f:
            pickle.dump(valsubsetmasks, f, protocol=4)

        vallist = (np.asarray(lesionlist)[shuffle_array[0:testpreps]])
        testsubsetmasks = dataset_lesion_masks[..., shuffle_array[testpreps:]]
        with open(os.path.join(opt.saveroot, 'test_masks_'+opt.dataname), "wb") as f:
            pickle.dump(testsubsetmasks, f, protocol=4)
        testlist = (np.asarray(lesionlist)[shuffle_array[testpreps:]])

    if not os.path.isfile(os.path.join(opt.saveroot, 'testdata_' + opt.dataname + test_file_extension)):
        if opt.hdf5testsampling == True:
            print('splitting set')
            splits = np.arange(0, len(testlist), 50)
            if not len(testlist)%50 == 0:
                splits = np.append(splits, len(testlist))
            chunks = [testlist[x:y] for x, y  in zip(splits[:-1], splits[1:])]
            curchunkindex = 0
            for chunklist in chunks:
                chunktestsubsetmask = testsubsetmasks[..., splits[curchunkindex]:splits[curchunkindex+1]]

                testdata, testtarget, testtargetsegmentation, testorigin = datasetgeneration(chunklist, chunktestsubsetmask, np.array([opt.patchsize, opt.patchsize]), opt, trainortest='test', nr_of_patches=opt.nrofpatches[2])

                test = {'data_'+str(curchunkindex): testdata, 'target_' + str(curchunkindex):testtarget, 'targetsegmentation_' + str(curchunkindex): testtargetsegmentation, 'origin_' + str(curchunkindex):testorigin, 'fullpatientlist_' + str(curchunkindex):chunklist.astype('S')}
                if curchunkindex == 0:
                    writeappend = 'w'
                else:
                    writeappend = 'a'
                with h5py.File(os.path.join(opt.saveroot, 'testdata_' + opt.dataname + '.hdf5'), writeappend) as f:
                    for k, v in test.items():
                        f.create_dataset(k, data=v)

                curchunkindex = curchunkindex + 1
                test = []
                testdata = []
        else:
            testdata, testtarget, testtargetsegmentation, testorigin = datasetgeneration(testlist, testsubsetmasks, np.array([opt.patchsize, opt.patchsize]), opt, trainortest='test', nr_of_patches=opt.nrofpatches[2])
            test = {'data': testdata, 'target':testtarget, 'targetsegmentation': testtargetsegmentation, 'origin':testorigin, 'fullpatientlist':testlist, 'opts':opt}
            with open(os.path.join(opt.saveroot, 'testdata_' + opt.dataname), "wb") as f:
                pickle.dump(test, f, protocol=4)

        dataset_lesion_masks = []


    if not os.path.isfile(os.path.join(opt.saveroot, 'valdata_'+opt.dataname)):
        valdata, valtarget, valtargetsegmentation, valorigin = datasetgeneration(vallist, valsubsetmasks, np.array([opt.patchsize, opt.patchsize]), opt, trainortest='test', nr_of_patches=opt.nrofpatches[1])
        validation = {'data': valdata, 'target':valtarget, 'targetsegmentation': valtargetsegmentation, 'origin':valorigin, 'fullpatientlist':vallist}
        with open(os.path.join(opt.saveroot, 'valdata_' + opt.dataname), "wb") as f:
            pickle.dump(validation, f, protocol=4)
        valdata, valtarget, valtargetsegmentation, valorigin = datasetgeneration(vallist, valsubsetmasks, np.array([opt.patchsize, opt.patchsize]), opt, trainortest='test', nr_of_patches=np.inf)
        validation2 = {'data': valdata, 'target':valtarget, 'targetsegmentation': valtargetsegmentation, 'origin':valorigin, 'fullpatientlist':vallist}
        with open(os.path.join(opt.saveroot, 'valdata_all_' + opt.dataname), "wb") as f:
            pickle.dump(validation2, f, protocol=4)
    else:
        validation = pickle.load(open(os.path.join(opt.saveroot, 'valdata_' + opt.dataname), "rb"))
        validation2 = pickle.load(open(os.path.join(opt.saveroot, 'valdata_all_' + opt.dataname), "rb"))

    lesionlist = []

    splits = ['train', 'trainvalidation', 'validation', 'validation_all']
    drop_last_batch = {'train': True, 'trainvalidation': True, 'validation': False, 'validation_all': False}
    shuffle = {'train': opt.shuffle_train, 'trainvalidation': False, 'validation': False, 'validation_all': False}
    dataset = {k:Dataset_SMART() for k in splits}
    dataset['train'].data = train['data'][:, 0:opt.nc, ...]
    dataset['train'].targets = np.squeeze(train['target'])
    dataset['trainvalidation'].data = trainval['data'][:, 0:opt.nc, ...]
    dataset['trainvalidation'].targets = np.squeeze(trainval['target'])
    dataset['validation'].data = validation['data'][:, 0:opt.nc, ...]
    dataset['validation'].targets = np.squeeze(validation['target'])
    dataset['validation_all'].data = validation2['data'][:, 0:opt.nc, ...]
    dataset['validation_all'].targets = np.squeeze(validation2['target'])
    dataset['validation_all'].origin = np.squeeze(validation2['origin'])
    train = ''
    validation = ''
    dataloader = {x: torch.utils.data.DataLoader(dataset=dataset[x],
                                                 batch_size=opt.batchsize,
                                                 shuffle=shuffle[x],
                                                 num_workers=int(opt.workers),
                                                 drop_last=drop_last_batch[x],
                                                 worker_init_fn=(None if opt.manualseed == -1
                                                 else lambda x: np.random.seed(opt.manualseed)))
                  for x in splits}

    return dataloader, opt

def load_testdata(opt, cur_chunk=0):
    """ Loads the test data during test time. If stored as hdf5 format, will load it as such
    """
    if not opt.hdf5testsampling == True:
        test = pickle.load(open(os.path.join(opt.saveroot, 'testdata_'+opt.dataname), "rb"))
        splits = ['test']
        drop_last_batch = {'test': False}
        shuffle = {'test': False}
        dataset = {k:Dataset_SMART() for k in splits}

        dataset['test'].data = test['data'][:, 0:opt.nc, ...]
        dataset['test'].targets = np.squeeze(test['target'])
        dataset['test'].origin = np.squeeze(test['origin'])
        test = ''

    else:
        splits = ['test']
        drop_last_batch = {'test': False}
        shuffle = {'test': False}
        dataset = {k:Dataset_SMART() for k in splits}
        with h5py.File(os.path.join(opt.saveroot, 'testdata_'+opt.dataname+'.hdf5'), 'r') as f:
            dataset['test'].data = f['data_'+str(cur_chunk)][()]/2
            dataset['test'].targets = np.squeeze(f['target_'+str(cur_chunk)][()])
            dataset['test'].origin = np.squeeze(f['origin_'+str(cur_chunk)][()])
        test = ''

    dataloader = {x: torch.utils.data.DataLoader(dataset=dataset[x],
                                                 batch_size=opt.batchsize,
                                                 shuffle=shuffle[x],
                                                 num_workers=int(opt.workers),
                                                 drop_last=drop_last_batch[x],
                                                 worker_init_fn=(None if opt.manualseed == -1
                                                 else lambda x: np.random.seed(opt.manualseed)))
                    for x in splits}
    return dataloader, opt

def datasetgeneration(patientfilelist, samplemasks, patchsize, opt, trainortest='train', nr_of_patches=np.inf):
    """ Function that performs the patch sampling
    INPUT:      patientfilelist:   List of patients to sample from
                samplemasks:       Masks in which sampling is performed. Areas where mask is zero are excluded.
                patchsize:         The requested patchsize
                opt:               Options dictionary
                trainortest:       Current neural network stage 'train' or 'test'(default)
                nr_of_patches:     Number of patches (np.inf, default)
                
    OUTPUT:     patches:           Array containing the sampled patches
                label:             0 if normal patch, 1 if contains anomaly
                patchesmask:       Mask containing the infarct prob map at image patch locations
                sampleorigin:      Top left coordinates of all image patches
    """
    patchnumber = 0
    imageShape = samplemasks.shape
    sampleall = 0
    if nr_of_patches == np.inf:
        sampleall = 1

    if not sampleall:
        brainslice_indices = np.transpose(np.vstack(np.nonzero(samplemasks[:, :, :, 0, :])).astype('uint16'))
    #thin out brainslice_indices
        for thinning in range(0, 6):
            brainslice_indices = np.delete(brainslice_indices, list(range(0, brainslice_indices.shape[0], 2)), axis=0)
        weights = samplemasks[brainslice_indices[:, 0], brainslice_indices[:, 1], brainslice_indices[:, 2], 0, brainslice_indices[:, 3]]
    else:
        brainslices = np.max(np.max(samplemasks[:, :, :, 0, ...], axis=0), axis=0)
        brainslice_indices = np.argwhere(np.squeeze(brainslices) > 0).astype('uint16')

    if sampleall:
        sample_locations_sliding_window = sliding_window(opt)
    else:
        sample_locations_sliding_window = np.array([0])

    if patchsize[0] == imageShape[0] or not sampleall:
        if trainortest == 'test':
            lesionslice_indices = np.transpose(np.vstack(np.nonzero(samplemasks[:, :, :, 1, :])).astype('int16'))
            lesionslice_indices_unique = np.unique(lesionslice_indices[:, 2:], axis=0)
            similar_slices = np.array(np.array(np.all((brainslice_indices[:, None, 2:] == lesionslice_indices_unique[None, :, :]), axis=-1).nonzero()).T.tolist())
            brainslice_indices = np.delete(brainslice_indices, similar_slices[:, 0], axis=0)


    elif trainortest == 'test':
        lesionslices = np.max(np.max(samplemasks[:, :, :, 1, ...], axis=0), axis=0)
        lesionslice_indices = np.argwhere(np.squeeze(lesionslices) > 0)

            #
    if sampleall:
        brainslice_indices = np.hstack((np.tile(sample_locations_sliding_window, (len(brainslice_indices), 1)), np.repeat(brainslice_indices, len(sample_locations_sliding_window), 0)))

        nr_of_patches = (brainslice_indices.shape[0])

    nr_of_patches = int(nr_of_patches)
    samplecoordinates = np.zeros([3, patchsize[0], patchsize[1], 1, nr_of_patches], dtype='uint16')
    sampleorigin = np.zeros((nr_of_patches, 4), dtype='int16')
    target = np.zeros((nr_of_patches, 1), dtype='int8')
    hasbrain = np.zeros((nr_of_patches, 1), dtype='int8')
    if not sampleall and trainortest == 'train':
        selected_mask_indices = np.random.choice(brainslice_indices.shape[0], nr_of_patches + 10000, p=weights / np.sum(weights))

    while patchnumber < nr_of_patches:

        if trainortest == 'train':
            if sampleall:
                selected_mask_index = patchnumber
            else:
                selected_mask_index = selected_mask_indices[patchnumber]

            cur_index = 0
        else:
            if sampleall:
                selected_mask_index = patchnumber

                cur_index = 0
            else:
                if np.random.choice([0, 1], p=[opt.proportion, 1 - opt.proportion]):
                    selected_mask_index = random.randrange(brainslice_indices.shape[0])
                    cur_index = 0
                else:
                    selected_mask_index = random.randrange(lesionslice_indices.shape[0])

                    cur_index = 1
        if cur_index == 0 or sampleall:
            x_origin = brainslice_indices[selected_mask_index, 0]
            y_origin = brainslice_indices[selected_mask_index, 1]

            patientorigin = brainslice_indices[selected_mask_index, -1]
            z = brainslice_indices[selected_mask_index, -2]
        else:
            x_origin = lesionslice_indices[selected_mask_index, 0]
            y_origin = lesionslice_indices[selected_mask_index, 1]

            patientorigin = lesionslice_indices[selected_mask_index, -1]
            z = lesionslice_indices[selected_mask_index, -2]

        x1 = x_origin - (patchsize[0]-1)//2
        y1 = y_origin - (patchsize[1]-1)//2

        if x1 >= 0 and y1 >= 0:

            x2 = x1 + patchsize[0]
            y2 = y1 + patchsize[1]

            brainmask_patch = samplemasks[x1:x2, y1:y2, z, cur_index, patientorigin]

            if np.max(brainmask_patch) > 0 or sampleall:
                if sampleall:
                    hasbrain[patchnumber] = int(np.mean(samplemasks[x1:x2, y1:y2, z, 0, patientorigin]) > 0.6)
                    if hasbrain[patchnumber]:
                        target[patchnumber] = int(np.max(np.max(samplemasks[x1:x2, y1:y2, z, 1, patientorigin])) > 0)
                        x = np.linspace(x1, x2-1, patchsize[0])
                        y = np.linspace(y1, y2-1, patchsize[1])
                        samplecoordinates[..., patchnumber] = np.array(np.meshgrid(x, y, z))
                        sampleorigin[patchnumber, 0] = x_origin
                        sampleorigin[patchnumber, 1] = y_origin
                        sampleorigin[patchnumber, 2] = z
                        sampleorigin[patchnumber, 3] = patientorigin
                else:
                    x = np.linspace(x1, x2-1, patchsize[0])
                    y = np.linspace(y1, y2-1, patchsize[1])
                    samplecoordinates[..., patchnumber] = np.array(np.meshgrid(x, y, z))
                    target[patchnumber] = cur_index
                    sampleorigin[patchnumber, 0] = x_origin
                    sampleorigin[patchnumber, 1] = y_origin
                    sampleorigin[patchnumber, 2] = z
                    sampleorigin[patchnumber, 3] = patientorigin
                patchnumber = patchnumber+1
        elif sampleall:
            print('Probably stuck in an infinite loop')
    #remove patches that do not contain brain voxels
    if sampleall:
        target = target[np.where(hasbrain == 1)[0], :]
        sampleorigin = sampleorigin[np.where(hasbrain == 1)[0], :]
        samplecoordinates = samplecoordinates[..., np.where(hasbrain == 1)[0]]
        nr_of_patches = target.shape[0]

    #target label: (0) = normal (1) = abnormal
    patch = np.zeros([patchsize[0], patchsize[1], opt.nc, nr_of_patches], dtype='float32')
    target_segmentation = np.zeros([patchsize[0], patchsize[1], 1, nr_of_patches], dtype='uint8')

    for scan in tqdm(range(len(patientfilelist)), total=len(patientfilelist), desc="Interpolating from image", leave=True):
        selectedpatches = np.transpose(np.array(np.where(sampleorigin[:, 3] == scan)))
        selectedpatchescoords = np.squeeze(samplecoordinates[..., selectedpatches[:, 0]])
        if not selectedpatches.size == 0:
            if selectedpatches.size == 1:
                selectedpatchescoords = selectedpatchescoords[..., np.newaxis]
            if selectedpatches.size:
                loadedscan_t1w = (nibabel.load(patientfilelist[scan] + '/reg_T1WFFE_T2WFLAIR_normalized.nii.gz')).get_fdata()
                loadedscan_t2w = (nibabel.load(patientfilelist[scan] + '/T2WFLAIR_normalized.nii.gz')).get_fdata()
                patch[:, :, 0, selectedpatches[:, 0]] = scipy.ndimage.interpolation.map_coordinates(loadedscan_t1w, selectedpatchescoords, order=1)
                patch[:, :, 1, selectedpatches[:, 0]] = scipy.ndimage.interpolation.map_coordinates(loadedscan_t2w, selectedpatchescoords, order=1)
                if opt.nc == 3:
                    loadedscan_t1wir = (nibabel.load(patientfilelist[scan] + '/reg_T1WIR_T2WFLAIR_normalized.nii.gz')).get_fdata()
                    patch[:, :, 2, selectedpatches[:, 0]] = scipy.ndimage.interpolation.map_coordinates(loadedscan_t1wir, selectedpatchescoords, order=1)
                if trainortest == 'test':
                    loadedscan_probmap = (nibabel.load(patientfilelist[scan] + '/Probmap7_infarct.nii.gz')).get_fdata()
                    target_segmentation[:, :, 0, selectedpatches[:, 0]] = scipy.ndimage.interpolation.map_coordinates(loadedscan_probmap, selectedpatchescoords, order=1)

    samplecoordinates = [0]
    return np.transpose(patch, (3, 2, 0, 1)), target, np.transpose(target_segmentation, (3, 2, 0, 1)), sampleorigin

def sliding_window(opt):
    """ sliding window"""
    x_locations = np.arange(0, opt.isize, (opt.patchsize-1)//opt.slidingwindowstride)

    if x_locations.shape[0] == 2 or opt.patchsize % opt.isize:
        x_locations = x_locations[:-1]
    x_locations[-1] = opt.isize-opt.patchsize
    x_locations = x_locations+(opt.patchsize-1)//2
    y_locations = np.arange(0, opt.isize, (opt.patchsize-1)//opt.slidingwindowstride)

    if y_locations.shape[0] == 2 or opt.patchsize % opt.isize:
        y_locations = y_locations[:-1]
    y_locations[-1] = opt.isize-opt.patchsize
    y_locations = y_locations+(opt.patchsize-1)//2
    xx, yy = np.meshgrid(x_locations, y_locations)

    return np.concatenate((xx[..., None].astype(int), yy[..., None].astype(int)), axis=2).reshape(-1, 2)


class Dataset_SMART(object):
    """An abstract class representing a Dataset.

    All other datasets should subclass it. All subclasses should override
    ``__len__``, that provides the size of the dataset, and ``__getitem__``,
    supporting integer indexing in range from 0 to len(self) exclusive.
    """

    def __getitem__(self, index):
        data = self.data[index, ...]
        targets = self.targets[index]
        return data, targets

    def __len__(self):
        return self.data.shape[0]
