"""
Projects the anomaly scores back into image space.

GUI requires image patches in .npy format, and results as '..._feature_zscores.npy' or '..._results.npy'.
The threshold option (default:3) is the Z-score threshold used to calculate the number of suspected anomalies.
Percentile (default:0.5==50th percentile) is used when a '..._feature_zscores.npy' file is given. In this case, the Nth percentile is calculated over all Z-scores per image patch.


KM van Hespen, UMC Utrecht, 2020

"""
import h5py
import numpy as np
import os
import pickle
import scipy.ndimage as ndimage

def init():
  ctx.field("itkImageFileReader.close").touch()
  ctx.field("itkImageFileReader.unresolvedFileName").setValue('')
  
def load():
  # loads the test/validation patient list, and calls all other functions
  global ptlist, origin, errors, seppath, found, itt, hdf
  itt = 0
  ctx.field("prbar_loading_images").setValue(0)
  
  if '.hdf5' in ctx.field("imagename").value:
    hdf = True
    with h5py.File(ctx.field("imagename").value, 'r') as f:
      nr_of_chunks = len(f.keys()) // 5
      for cur_chunk in range(0, nr_of_chunks):
        curptlist = np.squeeze(f['fullpatientlist_' + str(cur_chunk)][()])
        curorigin = np.squeeze(f['origin_' + str(cur_chunk)][()])
        
        if cur_chunk == 0:
          ptlist = curptlist
          origin = curorigin 
        else:
          curorigin = np.concatenate((curorigin[..., :-1], (curorigin[..., -1] + prevchunksize)[..., None]), 1)
          ptlist = np.concatenate((ptlist, curptlist), 0)
          origin = np.concatenate((origin, curorigin), 0)
        
        prevchunksize = ptlist.shape[0]
  
  else:
    hdf = False
    patches = pickle.load(open(ctx.field("imagename").value, "rb"))
    ptlist = patches['fullpatientlist']
    origin = patches['origin']
    patches=[]
  ctx.field("prbar_loading_images").setValue(0.5)
  errors = np.load(ctx.field("resultsname").value)
  ctx.field("FileInformation.path").setValue(ctx.field("resultsname").value)
  found=False
  seppath = (ctx.field("resultsname").value).rsplit('/')[-4]
  update()
  ctx.field("prbar_loading_images").setValue(0.75)
  compute_all_error_maps()
  ctx.field("prbar_loading_images").setValue(1)
  cleaned_comp()

def update():
  # Shows the anomaly score map of the current patient
  global ptlist, origin, errors, found, itt, extent, hdf
  curpt = ctx.field("curpt").intValue()
  try:
    ctx.field("StringUtils.string1").setValue(ptlist[curpt].decode('UTF-8'))
  except:
    ctx.field("StringUtils.string1").setValue(ptlist[curpt])
  ctx.field("SubImage1.c").setValue(curpt)  
  T1W = ctx.field("itkImageFileReader.output0").image()
  T1W = T1W.getTile( (0, 0, 0, 0, 0), (T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent) )
  
  cur_origin = np.squeeze(origin[np.where(origin[:, 3] == curpt), ...])
  cur_error = np.squeeze(errors[np.where(origin[:, 3] == curpt), ...])
  
  if 'zscores' in (ctx.field("resultsname").value):
    cur_error = np.percentile(cur_error, ctx.field('percentile').value * 100, axis = 1)
  else:
    cur_error = cur_error[:, 2]
  
  temp_img = np.zeros(T1W.shape)
  #Tries to find the stride used in patch sampling, by looking for the closest patch
  while found == False:
    similarsliceindices = np.where((origin[:, 2] == origin[itt, 2]) * (origin[:, 3] == origin[itt, 3]))[0]
    
    if similarsliceindices.shape[0] > 20:
      extent = np.sort(np.sqrt(np.sum(np.power(origin[similarsliceindices, 0:2] - origin[itt, 0:2], 2), axis = 1)))[1] // 2
      extent = extent.astype(int)
      found = True
    else:
      itt = itt + 1
      
    if itt > 300:
      print('something wrong with extent calculation')
      extent = 4
      break
  # Projects anomaly score in original image space  
  for origin_point in range(0, cur_origin.shape[0]):
     temp_img[..., cur_origin[origin_point, 2], cur_origin[origin_point, 1] - extent:cur_origin[origin_point, 1] + extent + 1, cur_origin[origin_point, 0] - extent:cur_origin[origin_point, 0] + extent + 1] = cur_error[origin_point]

  interface = ctx.module("PythonImage1").call("getInterface")  
  interface.setImage(temp_img)
  ctx.field("SubImage1.c").setValue(ctx.field("curpt").value)
  
def compute_all_error_maps():
  # Computes the anomaly score maps for all patients in the current data set.
  global ptlist, origin, errors, seppath, extent, lesion_original_masks, labeled_original, ncomponents_original, hdf
  if 'test' in ctx.field("imagename").value:
    lesion_original_masks = pickle.load(open(os.path.join(os.path.dirname(ctx.field("imagename").value), 'test_masks_' + seppath), "rb"))
  else:
    lesion_original_masks = pickle.load(open(os.path.join(os.path.dirname(ctx.field("imagename").value), 'val_masks_' + seppath), "rb"))  
    
  lesion_original_masks = lesion_original_masks[..., 1, :] > 0
  lesion_original_masks = reconstruction_by_erosion(lesion_original_masks, struct = np.ones((3, 3, 1, 1)), struct_dilate = np.ones((3, 3, 3, 1)))
  labelstruct = ndimage.morphology.generate_binary_structure(lesion_original_masks.ndim, 4)
  labelstruct[..., 0] = False
  labelstruct[..., 2] = False
  labeled_original, ncomponents_original = ndimage.measurements.label(lesion_original_masks, structure = labelstruct)
  labeled_original  = (labeled_original * lesion_original_masks).astype('uint16')
  # If zscores is not in the results file name, the 'old' calculation of z-scores as proposed by Akcay is assumed. In other cases the anomaly score is calculated by taking the Nth percentile of z-score values
  if 'zscores' in (ctx.field("resultsname").value):
    cur_error = np.percentile(errors,ctx.field('percentile').value * 100, axis = 1)
  else:
    cur_error = errors[:, 2]

  T1W = ctx.field("itkImageFileReader.output0").image()
  T1W = T1W.getTile( (0, 0, 0), (T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent) )
  temp_img = np.zeros((256, 256, 38) + (np.max(origin[:, 3]) + 1,))
  
  for origin_point in range(0,origin.shape[0]):
    temp_img[origin[origin_point, 0] - extent:origin[origin_point, 0] + extent + 1, origin[origin_point, 1] - extent:origin[origin_point, 1] + extent + 1,origin[origin_point, 2],origin[origin_point, 3]] = cur_error[origin_point] 

  lesion_mask = temp_img > ctx.field("threshold").value
  temp_img = []
  labelstruct = ndimage.morphology.generate_binary_structure(lesion_mask.ndim, 4)
  labelstruct[..., 0] = False
  labelstruct[..., 2] = False
  labeled_guesses, ncomponents = ndimage.measurements.label(lesion_mask, structure = labelstruct)
  labeled_guesses = labeled_guesses.astype('uint16')
  interface = ctx.module("All_error_maps").call("getInterface")  
  interface.setImage(np.transpose(labeled_guesses, (3, 2, 1, 0)))
  uniques = np.unique(labeled_original * lesion_mask)
  guesses = uniques.shape[0]  
  guesses2 = np.unique(lesion_original_masks * labeled_guesses).shape[0]
  tp = guesses
  fn = ncomponents_original - guesses
  fp = ncomponents - (guesses2 - 1)
  print('   true lesions: %i, estimated lesions: %i, TP: %i, FP: %i, FN: %i, sens: %.2f, prec: %.2f' % (ncomponents_original, ncomponents, tp, fp ,fn, tp / (tp + fn), tp / (tp + fp)))
  
def reconstruction_by_erosion(image,struct = np.ones((7, 7, 7, 1)), struct_dilate = np.ones((7, 7, 7, 1))):
  #Reconstruction by erosion
  eroded_square = ndimage.binary_erosion(image, structure = struct)
  reconstruction = ndimage.binary_propagation(eroded_square, structure = struct_dilate, mask = image)
  return reconstruction

def reconstruction_by_dilation(image,struct = np.ones((5, 5, 1, 1)),struct_erode = np.ones((5, 5, 1, 1))):
  #Reconstruction by dilation
  eroded_square = ndimage.binary_dilation(image, structure = struct)
  reconstruction = ndimage.binary_erosion(eroded_square, structure = struct_erode)
  return reconstruction
  
def cleaned_comp():
  # Cleans the thresholded error maps, by filtering out spurious activation. In addition, the volume of the missed and detected infarcts is computed.
  # For the volume computations the corresponding volumes.......npy file is used (file is located in the same directory as the image patches)
  global ptlist, origin, errors, seppath, extent, lesion_original_masks, labeled_original, ncomponents_original, ptlist, hdf
  T1W = ctx.field("All_error_maps.output0").image()
  T1W = T1W.getTile( (0, 0, 0, 0), (T1W.UseImageExtent,T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent) )
  uniques = np.unique(T1W)
  temp = np.zeros(T1W.shape)
  
  T1Wtemp = T1W
  holdvolume = np.zeros((uniques.shape[0] + 1, 1)) 
  holdvolume= ndimage.measurements.labeled_comprehension(T1Wtemp > 0, T1W, np.linspace(1, uniques.shape[0] - 1,uniques.shape[0] - 1), np.sum,  np.uint32, 0)

  indices = np.argwhere(holdvolume <= (np.power((extent) * 2 + 1, 2)) * ctx.field("sizethresh").value) 
  np.save(ctx.field("StringUtils3.string1").value + 'patientlist.npy', ptlist)
  indices = indices + 1
  lesion_mask = np.isin(T1W,np.append(indices, 0), invert = True)
  labelstruct = ndimage.morphology.generate_binary_structure(lesion_mask.ndim, 4)
  labelstruct[0, ...] = False
  labelstruct[2, ...] = False
  labeled_guesses, ncomponents = ndimage.measurements.label(lesion_mask, structure = labelstruct)
  interface = ctx.module("PythonImage2").call("getInterface")  
  interface.setImage(labeled_guesses)
  guesses = np.unique(labeled_original * np.transpose(lesion_mask, (3, 2, 1, 0)))
  guesses2 = np.unique(lesion_original_masks * np.transpose(labeled_guesses, (3, 2, 1, 0)))

  volumes = np.zeros((np.unique(labeled_original).shape[0],))
  
  if os.path.isfile(os.path.join(os.path.dirname(ctx.field("imagename").value), 'volumes_' + seppath + '.npy')):
    volumes = np.load(os.path.join(os.path.dirname(ctx.field("imagename").value), 'volumes_' + seppath + '.npy'))
  else:
    
    for uni in range(0, np.unique(labeled_original).shape[0]):
      indices = np.where(labeled_original == uni)
      patientind = np.max(indices[-1])
      volumes[uni]=np.sum((labeled_original[..., patientind]) == uni)
    np.save(os.path.join(os.path.dirname(ctx.field("imagename").value), 'volumes_' + seppath + '.npy'), volumes)
    
  volumesguessed = volumes[guesses]
  volumesguessed = volumesguessed[1:]
  volumes[guesses] = -1
  volumesmissed = volumes[volumes != -1]
  print('missed volume fraction:')
  print(np.sum(volumesmissed) / (np.sum(volumesguessed) + np.sum(volumesmissed)))
  print('missed volume:')
  print(np.array2string(volumesmissed).replace('\n', '').replace('  ', ' ').replace('   ', ' ').replace('[', '').replace(']', ''))
  print('detected volume: ')
  print(np.array2string(volumesguessed).replace('\n', '').replace('  ', ' ').replace('   ', ' ').replace('[', '').replace(']', ''))
  
  for guess in guesses2[1:]:
    labeled_guesses[labeled_guesses == guess] = 0  
  
  interface = ctx.module("FalsePositives").call("getInterface")  
  interface.setImage(labeled_guesses)      
  tp = guesses.shape[0] - 1
  fn = ncomponents_original - tp
  fp = ncomponents - (guesses2.shape[0] - 1)

  print('   true cleaned? lesions: %i, estimated lesions: %i, TP: %i, FP: %i, FN: %i, sens: %.2f, prec: %.2f' % (ncomponents_original, ncomponents, tp, fp, fn, tp / (tp + fn), tp / (tp + fp)))

def saveerrormaps():
  #Saves the error maps
  ctx.field("FileInformation.path").setValue(ctx.field("resultsname").value)
  ctx.field("itkImageFileWriter.save").touch()