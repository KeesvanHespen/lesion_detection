"""
Viewer for the nearest neighbor results of the anomaly detection neural network.
One View2D screen shows a infarct patch, the other 5 show the 5 nearest neighbor training image patches.

KM van Hespen, UMC Utrecht, 2020
"""
import glob
from matplotlib.animation import ArtistAnimation
import matplotlib.pyplot as plt 
import matplotlib as mpl
import numpy as np
import os
import pickle
import subprocess as subp

def Load():
  # Loads the training image patches and the test set image patches
  global sorted, patch_imgs, segment, segment_hold, patch_imgs_hold, ptlist, origin_FN,loaded, holdptnumber, errorsall, origin, patchestrain, errors
  sorted = 0
  CTX = ctx # ctx is the network's context, not RunPythonScript's
  
  if len(ctx.field("imagename").value) > 0:
    patches = pickle.load(open(ctx.field("imagename").value, "rb"))
    patchestrain = pickle.load(open(ctx.field("imagename").value.replace('valdata_all','traindata'),"rb"))
    patchestrain = patchestrain['data']

    ptlist = patches['fullpatientlist']
    origin = patches['origin']
    errors = np.load(ctx.field("resultsname").value).astype(int)
    errorsall = np.load(ctx.field("resultsname2").value)
    ctx.field("curpt").setValue(0)
    origin_FN = origin[errors[:, 0].astype(int), ...]
    ctx.field("itkImageFileReader.unresolvedFileName").setValue(os.path.join(ptlist[origin_FN[0, -1]], 'T2WFLAIR_normalized.nii.gz'))
    ctx.field("itkImageFileReader1.unresolvedFileName").setValue(os.path.join(ptlist[origin_FN[0, -1]], 'Probmap7_infarct.nii.gz'))
    ctx.field("WorldVoxelConvert.voxelX").setValue(origin_FN[0, 0])
    ctx.field("WorldVoxelConvert.voxelY").setValue(origin_FN[0, 1])
    ctx.field("WorldVoxelConvert.voxelZ").setValue(origin_FN[0, 2])
    ctx.field("SubImage.z").setValue(origin_FN[0, 2])
    ctx.field("SubImage2.z").setValue(origin_FN[0, 2])

    T1W = ctx.field("itkImageFileReader.output0").image()
    T1W = T1W.getTile( (0, 0, 0, 0, 0), (T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent) )
    holdptnumber = origin_FN[0, 3]
    loaded = 1
    cur_origin = np.squeeze(origin[np.where(origin[:, 3] == origin_FN[0, 3]), ...])
    cur_error = np.squeeze(errorsall[np.where(origin[:, 3] == origin_FN[0, 3]), :])
    temp_img = np.zeros(T1W.shape)

    for origin_point in range(0,cur_origin.shape[0]):
      temp_img[..., cur_origin[origin_point, 2], cur_origin[origin_point, 1] - 4:cur_origin[origin_point, 1] + 4, cur_origin[origin_point, 0] - 4:cur_origin[origin_point, 0] + 4] = cur_error[origin_point, 2]
  
    interface = ctx.module("PythonImage1").call("getInterface")  
    interface.setImage(temp_img)
    ctx.field("SubImage3.z").setValue(origin_FN[0, 2])   
    interface = ctx.module("PythonImage2").call("getInterface")  
    interface.setImage(patchestrain[errors[0, 1], ...])
    interface = ctx.module("PythonImage3").call("getInterface")  
    interface.setImage(patchestrain[errors[0, 2], ...])
    interface = ctx.module("PythonImage4").call("getInterface")  
    interface.setImage(patchestrain[errors[0, 3], ...])
    interface = ctx.module("PythonImage5").call("getInterface")  
    interface.setImage(patchestrain[errors[0, 4], ...])
    interface = ctx.module("PythonImage6").call("getInterface")  
    interface.setImage(patchestrain[errors[0, 5], ...])
    
def update():
  # Updates the currently shown test set image patch, and training set image patches
  global ptlist, origin_FN, loaded, holdptnumber, errorsall, origin, patchestrain, errors
  
  if loaded == 1:
    if origin_FN[ctx.field("curpt").value,3] != holdptnumber:
      T1W = ctx.field("itkImageFileReader.output0").image()
      T1W = T1W.getTile( (0, 0, 0, 0, 0), (T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent, T1W.UseImageExtent) )
      ctx.field("itkImageFileReader.unresolvedFileName").setValue(os.path.join(ptlist[origin_FN[ctx.field("curpt").value, -1]], 'T2WFLAIR_normalized.nii.gz'))
      ctx.field("itkImageFileReader1.unresolvedFileName").setValue(os.path.join(ptlist[origin_FN[ctx.field("curpt").value, -1]], 'Probmap7_infarct.nii.gz'))
      cur_origin = np.squeeze(origin[np.where(origin[:, 3] == origin_FN[0, 2]), ...])
      cur_error = np.squeeze(errorsall[np.where(origin[:, 3] == origin_FN[0, 2]), :])
      temp_img = np.zeros(T1W.shape)
      holdptnumber = origin_FN[ctx.field("curpt").value, 3]
      
      for origin_point in range(0, cur_origin.shape[0]):
        temp_img[..., cur_origin[origin_point, 2], cur_origin[origin_point, 1] - 4:cur_origin[origin_point,1] + 4, cur_origin[origin_point, 0] - 4:cur_origin[origin_point, 0] + 4] = cur_error[origin_point, 2]
    
      interface = ctx.module("PythonImage1").call("getInterface")  
      interface.setImage(temp_img)
      
    ctx.field("WorldVoxelConvert.voxelX").setValue(origin_FN[ctx.field("curpt").value, 0])
    ctx.field("WorldVoxelConvert.voxelY").setValue(origin_FN[ctx.field("curpt").value, 1])
    ctx.field("WorldVoxelConvert.voxelZ").setValue(origin_FN[ctx.field("curpt").value, 2])
    
    ctx.field("SubImage.z").setValue(origin_FN[ctx.field("curpt").value, 2])
    ctx.field("SubImage2.z").setValue(origin_FN[ctx.field("curpt").value, 2])
    ctx.field("SubImage3.z").setValue(origin_FN[ctx.field("curpt").value, 2])

    interface = ctx.module("PythonImage2").call("getInterface")  
    interface.setImage(patchestrain[errors[ctx.field("curpt").value, 1], ...])
    interface = ctx.module("PythonImage3").call("getInterface")  
    interface.setImage(patchestrain[errors[ctx.field("curpt").value, 2], ...])
    interface = ctx.module("PythonImage4").call("getInterface")  
    interface.setImage(patchestrain[errors[ctx.field("curpt").value, 3], ...])
    interface = ctx.module("PythonImage5").call("getInterface")  
    interface.setImage(patchestrain[errors[ctx.field("curpt").value, 4], ...])
    interface = ctx.module("PythonImage6").call("getInterface")  
    interface.setImage(patchestrain[errors[ctx.field("curpt").value ,5], ...])
    
def init():
  global loaded
  loaded=0
  ctx.field("itkImageFileReader.close").touch()
 