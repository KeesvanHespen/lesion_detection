
import sys
sys.path.append('..')
from pycore.tikzeng import *
from pycore.blocks  import *

arch = [ 
    to_head('..'), 
    to_cor(),
    to_begin(),
    
    #input
    

    #encoder
	to_Inputlayer( name='ccr_b00', s_filer='', n_filer=2, offset="(0,0,0)", to="(-3.7,0,0)", width=1, height=45, depth=45, caption= "x" ),
	to_ConvConvRelu( name='ccr_b0', s_filer='', n_filer=('',''), offset="(0,0,0)", to="(-3.5,0,0)", width=(3.25,0), height=0, depth=0  ),
	to_input( '../brain.jpg' ),
	
    to_ConvConvleakyRelu( name='ccr_b1', s_filer=7, n_filer=(250,''), offset="(0,0,0)", to="(0,0,0)", width=(2.5,0), height=30, depth=30  ),
	to_connection( "ccr_b0", "ccr_b1"),
	to_ConvBN2leakyRelu( name='ccr_b2', s_filer=3, n_filer=500, offset="(2,0,0)", to="(ccr_b1-east)", width=5, height=15, depth=15  ),
	to_connection( "ccr_b1", "ccr_b2"),
	to_Conv( name='ccr_b3', s_filer='', n_filer=400, offset="(2,0,0)", to="(ccr_b2-east)", width=5, height=1, depth=1, caption = "z"  ),
	#decoder
	to_connection( "ccr_b2", "ccr_b3"),   	
	to_ConvBN2Relu( name='ccr_b4', s_filer=3, n_filer=500, offset="(2,0,0)", to="(ccr_b3-east)", width=5, height=15, depth=15  ),
	to_connection( "ccr_b3", "ccr_b4"),
	to_ConvBN2Relu( name='ccr_b5', s_filer=7, n_filer=250, offset="(2,0,0)", to="(ccr_b4-east)", width=2.5, height=30, depth=30  ),
    to_connection( "ccr_b4", "ccr_b5"),
	to_Conv2Tanh( name='ccr_b6', s_filer=15, n_filer=(2,''), offset="(3.5,0,0)", to="(ccr_b5-east)", width=(1,0), height=45, depth=45, caption = "\^{x}"	),
	
	#encoder
	to_ConvConvleakyRelu( name='dcr_b1', s_filer=7, n_filer=(250,''), offset="($(7.25,0,0)+(ccr_b5-east)$)", to="($(3,0,0)+(ccr_b5-east)$)", width=(2.5,0), height=30, depth=30  ),
	to_connection( "ccr_b5", "ccr_b6"),
	#to_output( '../examples/fcn8s/cats.jpg',name='outputer', to = "(ccr_b5-east)", offset="(3,0,0)" ),
	to_connection( "ccr_b6", "dcr_b1"),
	to_ConvBN2leakyRelu( name='dcr_b2', s_filer=3, n_filer=500, offset="(2,0,0)", to="(dcr_b1-east)", width=5, height=15, depth=15  ),
	to_connection( "dcr_b1", "dcr_b2"),
	to_Conv( name='dcr_b3', s_filer='', n_filer=400, offset="(2,0,0)", to="(dcr_b2-east)", width=5, height=1, depth=1 , caption = "\^{z}" ),
	to_connection( "dcr_b2", "dcr_b3"),
	#discriminator
	to_ConvConvleakyRelu( name='ecr_b0', s_filer='', n_filer=('',''), offset="($(7.25,12,0)+(ccr_b5-east)$)", to="($(3,0,0)+(ccr_b5-east)$)", width=(0,0), height=0, depth=0  ),
	to_angled( "ccr_b0", "ecr_b0", pos=1.6),
	to_angled( "ccr_b6", "ecr_b0", pos=5.75),
    to_ConvConvleakyRelu( name='ecr_b1', s_filer=7, n_filer=(250,''), offset="($(7.25,12,0)+(ccr_b5-east)$)", to="($(3,0,0)+(ccr_b5-east)$)", width=(2.5,0), height=30, depth=30  ),
	to_ConvBN2leakyRelu( name='ecr_b2', s_filer=3, n_filer=500, offset="(2,0,0)", to="(ecr_b1-east)", width=5, height=15, depth=15  ),
	to_connection( "ecr_b1", "ecr_b2"),
	
	to_ConvSigmoid( name='ecr_b3', s_filer='', n_filer=1, offset="(2,0,0)", to="(ecr_b2-east)", width=2, height=1, depth=1 ),
	to_connection( "ecr_b2", "ecr_b3"),
    
    
	# legend
	to_freecolorConv( name='legend1', s_filer='', n_filer='', fillcolor="\InputColor", offset="(0,-14,0)", to="(ccr_b00-north)", width=3, height=5, depth=5, caption = "Input"  ),
	to_freecolorConv( name='legend2', s_filer='', n_filer='', fillcolor="\ConvColor", offset="(3.35,0,0)", to="(legend1-east)", width=3, height=5, depth=5, caption = "(Transpose) Conv        5x5"  ),
	to_freecolorConv( name='legend3', s_filer='', n_filer='', fillcolor="\ConvthreeColor", offset="(3.35,0,0)", to="(legend2-east)", width=3, height=5, depth=5, caption = "Conv 3x3"  ),
	to_freecolorConv( name='legend4', s_filer='', n_filer='', fillcolor="\ConvReluColor", offset="(3.35,0,0)", to="(legend3-east)", width=3, height=5, depth=5, caption = "ReLu"  ),
	to_freecolorConv( name='legend5', s_filer='', n_filer='', fillcolor="\ConvleakyReluColor", offset="(3.35,0,0)", to="(legend4-east)", width=3, height=5, depth=5, caption = "leaky ReLu"  ),
	to_freecolorConv( name='legend6', s_filer='', n_filer='', fillcolor="\ConvBNColor", offset="(3.35,0,0)", to="(legend5-east)", width=3, height=5, depth=5, caption = "BN"  ),
	to_freecolorConv( name='legend7', s_filer='', n_filer='', fillcolor="\ConvSigmoidColor", offset="(3.35,0,0)", to="(legend6-east)", width=3, height=5, depth=5, caption = "Sigmoid"  ),
	to_freecolorConv( name='legend8', s_filer='', n_filer='', fillcolor="\SoftmaxColor", offset="(3.35,0,0)", to="(legend7-east)", width=3, height=5, depth=5, caption = "Tanh"  ),

	
	to_end() 
    ]


def main():
    namefile = str(sys.argv[0]).split('.')[0]
    to_generate(arch, namefile + '.tex' )

if __name__ == '__main__':
    main()
    
