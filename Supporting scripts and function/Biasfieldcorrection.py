# -*- coding: utf-8 -*-
"""
Created on Thu Aug  8 16:20:09 2019

@author: khespen
"""
import glob
import os
import subprocess as subp
  
ANTSdir = '"C:\\Program Files\\ANTs\\Release\\N4BiasFieldCorrection"'
SMARTdir = 'D:\\10.SMART-Data\\SMART'

def Correct_bias_field(filenames,maskname):
    
    for selectedimage in filenames:
        directory, temp = os.path.split(selectedimage)
        
        if not os.path.isfile(selectedimage + '_biascorrected.nii.gz'):
          selectedimagepath, selectedimage = os.path.split(selectedimage)  
          if os.path.isfile(os.path.join(directory,maskname)):
              systemcall = subp.Popen(["powershell.exe",'cd "'+ selectedimagepath +'/"; docker run --rm -it -v ${pwd}:/kees kaczmarj/ants:2.2.0 N4BiasFieldCorrection ' + ' -i /kees/'+ selectedimage  +' -o /kees/'+ selectedimage +'_biascorrected.nii.gz' + ' -w /kees/'+maskname+' -s 2 -t [0.05] -c [100x100x100x100,0.000]'])
              systemcall.wait()
          else:
              systemcall = subp.Popen(["powershell.exe",'cd "'+ selectedimagepath +'/"; docker run --rm -it -v ${pwd}:/kees kaczmarj/ants:2.2.0 N4BiasFieldCorrection ' + ' -i /kees/'+ selectedimage  +' -o /kees/'+ selectedimage +'_biascorrected.nii.gz' + ' -s 2 -t [0.05] -c [100x100x100x100,0.000]'])
              systemcall.wait()             
            

    
T1name = 'reg_T1WFFE_T2WFLAIR.nii.gz'
T1_files = glob.glob(os.path.join(SMARTdir,'*',T1name),recursive=True)

Correct_bias_field(T1_files,'brainmask.nii.gz')
T2name='T2WFLAIR.nii.gz'
T2_files = glob.glob(os.path.join(SMARTdir,'*',T2name),recursive=True)
Correct_bias_field(T2_files,'brainmask.nii.gz')