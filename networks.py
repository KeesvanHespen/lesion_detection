""" Network architectures.
"""
# pylint: disable=W0221,W0622,C0103,R0913

##
import torch
import torch.nn as nn
import torch.nn.parallel

##
def weights_init(mod):
    """
    Custom weights initialization called on netG, netD and netE
    :param m:
    :return:
    """
    classname = mod.__class__.__name__
    if classname.find('Conv') != -1:
        mod.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        mod.weight.data.normal_(1.0, 0.02)
        mod.bias.data.fill_(0)

###
class Encoder(nn.Module):
    """
    DCGAN ENCODER NETWORK
    """

    def __init__(self, patchsize, nz, nc, ndf, ngpu, kernel_size, stride, padding, n_extra_layers=0, add_final_conv=True, dilate=1):
        super(Encoder, self).__init__()
        self.ngpu = ngpu
#        assert (patchsize+1) % 16 == 0, "patchsize has to be a multiple of 16 minus 1"

        main = nn.Sequential()
        # input is nc x isize x isize
        main.add_module('initial-conv-{0}-{1}'.format(nc, ndf),
                        nn.Conv2d(nc, ndf, kernel_size, stride, padding, bias=False, dilation=dilate))
        main.add_module('initial-relu-{0}'.format(ndf),
                        nn.LeakyReLU(0.2, inplace=True))
        csize, cndf = (patchsize - (dilate * (kernel_size - 1) + 1) + 2 * padding) / stride + 1, ndf

        # Extra layers
        for t in range(n_extra_layers):
            main.add_module('extra-layers-{0}-{1}-conv'.format(t, cndf),
                            nn.Conv2d(cndf, cndf, 3, 1, 1, bias=False))
            main.add_module('extra-layers-{0}-{1}-batchnorm'.format(t, cndf),
                            nn.BatchNorm2d(cndf))
            main.add_module('extra-layers-{0}-{1}-relu'.format(t, cndf),
                            nn.LeakyReLU(0.2, inplace=True))

        while csize > kernel_size:
            in_feat = cndf
            out_feat = cndf * 2
            main.add_module('pyramid-{0}-{1}-conv'.format(in_feat, out_feat),
                            nn.Conv2d(in_feat, out_feat, kernel_size, stride, padding, bias=False, dilation=dilate))
            main.add_module('pyramid-{0}-batchnorm'.format(out_feat),
                            nn.BatchNorm2d(out_feat))
            main.add_module('pyramid-{0}-relu'.format(out_feat),
                            nn.LeakyReLU(0.2, inplace=True))
            cndf = cndf * 2
            csize = (csize - (dilate * (kernel_size - 1) + 1) + 2 * padding) / stride + 1

            if cndf > 1000000:
                break

        # state size. K x 4 x 4
        if add_final_conv:
            main.add_module('final-{0}-{1}-conv'.format(cndf, 1),
                            nn.Conv2d(cndf, nz, int(csize), 1, 0, bias=False))

        self.main = main
        print('Encoder model has %i trainable parameters' % sum(p.numel() for p in main.parameters() if p.requires_grad))


    def forward(self, input):
        if self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else:
            output = self.main(input)

        return output

##
class Decoder(nn.Module):
    """
    DCGAN DECODER NETWORK
    """
    def __init__(self, patchsize, nz, nc, ngf, ngpu, kernel_size, stride, padding, n_extra_layers=0, dilate=1):
        super(Decoder, self).__init__()
        self.ngpu = ngpu
       # assert (patchsize+1) % 16 == 0, "patchsize has to be a multiple of 16 minus 1"

        csize = (patchsize-(dilate * (kernel_size - 1) + 1) + 2 * padding) / stride + 1
        while csize > kernel_size:
            csize = (csize-(dilate * (kernel_size - 1) + 1) + 2 * padding) / stride + 1

        cngf, tisize = ngf // 2, csize
        while tisize != patchsize:
            cngf = cngf * 2
            tisize = (tisize - 1) * stride + (dilate * (kernel_size - 1) + 1) - 2* padding

        main = nn.Sequential()
        # input is Z, going into a convolution
        main.add_module('initial-{0}-{1}-convt'.format(nz, cngf),
                        nn.ConvTranspose2d(nz, cngf, int(csize), 1, 0, bias=False))
        main.add_module('initial-{0}-batchnorm'.format(cngf),
                        nn.BatchNorm2d(cngf))
        main.add_module('initial-{0}-relu'.format(cngf),
                        nn.ReLU(True))

        while csize < (patchsize - (dilate * (kernel_size - 1) + 1) + 2 * padding) / stride + 1:
            main.add_module('pyramid-{0}-{1}-convt'.format(cngf, cngf // 2),
                            nn.ConvTranspose2d(cngf, cngf // 2, kernel_size, stride, padding, bias=False, dilation=dilate))
            main.add_module('pyramid-{0}-batchnorm'.format(cngf // 2),
                            nn.BatchNorm2d(cngf // 2))
            main.add_module('pyramid-{0}-relu'.format(cngf // 2),
                            nn.ReLU(True))
            cngf = cngf // 2
            csize = (csize-1) * stride + (dilate * (kernel_size - 1) + 1) - 2 * padding
            if cngf > 1000000:
                break

        # Extra layers
        for t in range(n_extra_layers):
            main.add_module('extra-layers-{0}-{1}-conv'.format(t, cngf),
                            nn.Conv2d(cngf, cngf, 3, 1, 1, bias=False))
            main.add_module('extra-layers-{0}-{1}-batchnorm'.format(t, cngf),
                            nn.BatchNorm2d(cngf))
            main.add_module('extra-layers-{0}-{1}-relu'.format(t, cngf),
                            nn.ReLU(True))

        main.add_module('final-{0}-{1}-convt'.format(cngf, nc),
                        nn.ConvTranspose2d(cngf, nc, kernel_size, stride, padding, bias=False, dilation=dilate))
        main.add_module('final-{0}-tanh'.format(nc),
                        nn.Tanh())
        self.main = main

    def forward(self, input):
        if self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else:
            output = self.main(input)
        return output


##
class NetD(nn.Module):
    """
    DISCRIMINATOR NETWORK
    """

    def __init__(self, opt):
        super(NetD, self).__init__()
        model = Encoder(opt.patchsize, 1, opt.nc, opt.ndf, opt.ngpu, opt.ks, opt.stride, opt.pad, opt.extralayers, dilate=opt.dilation)
        layers = list(model.main.children())

        self.features = nn.Sequential(*layers[:-1])
        self.classifier = nn.Sequential(layers[-1])
        self.classifier.add_module('Sigmoid', nn.Sigmoid())

    def forward(self, x):
        features = self.features(x)
        features = features
        classifier = self.classifier(features)
        classifier = classifier.view(-1, 1).squeeze(1)

        return classifier, features

##
class NetG(nn.Module):
    """
    GENERATOR NETWORK
    """

    def __init__(self, opt):
        super(NetG, self).__init__()
        self.encoder1 = Encoder(opt.patchsize, opt.nz, opt.nc, opt.ngf, opt.ngpu, opt.ks, opt.stride, opt.pad, opt.extralayers, dilate=opt.dilation)
        self.decoder = Decoder(opt.patchsize, opt.nz, opt.nc, opt.ngf, opt.ngpu, opt.ks, opt.stride, opt.pad, opt.extralayers, dilate=opt.dilation)
        self.encoder2 = Encoder(opt.patchsize, opt.nz, opt.nc, opt.ngf, opt.ngpu, opt.ks, opt.stride, opt.pad, opt.extralayers, dilate=opt.dilation)

    def forward(self, x):
        latent_i = self.encoder1(x)
        gen_imag = self.decoder(latent_i)
        latent_o = self.encoder2(gen_imag)
        return gen_imag, latent_i, latent_o
