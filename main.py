"""
TRAIN GANOMALY

    Adapted anomaly script and network to detect brain anomalies from brain MR images.

    Original by: S Akcay

    KM van Hespen 2019 UMC Utrecht

."""


##

# LIBRARIES
from __future__ import print_function
import numpy as np
from options import Options
from data import load_data
from model import Ganomaly

##
# def main():
""" Training
"""

##
# ARGUMENTS/*
opt = Options().parse()
opt.dataroot='./Shared/SMART/'
opt.saveroot = './Surfdrive'
opt.outf = './Surfdrive/'
opt.dataname = 'patches_15'
opt.experimentname = '15_nz50ngf100'
opt.skiplist = ['1027', '1059', '1028', '1097', '1151', '1103', '1067', '1186', '1230', '1250', '1317', '1369', '1402', '1557', '1570', '1589', '1602', '1606', '1609', '1612', '1650', '1662', '1708', '1712', '1758', '1780', '1790', '1933', '1982', '2001', '2006', '2016', '2024', '2034', '2037', '2051', '2213', '2203', '2242', '2357']
opt.load_weights = False
opt.shuffle_train = True
opt.display = True

#Architecture arguments
opt.nc = 2
opt.nz = 50
opt.ks = 5
opt.stride = 2
opt.pad = 1
opt.ngf = 100
opt.ndf = 64
opt.extralayers = 0
opt.dilation = 1

#Training arguments
opt.niter_max = 300
opt.niter_history = 25
opt.batchsize = 64
opt.lr = 1e-3
opt.w_con = 70
opt.w_enc = 10
opt.beta1 = 0.5

#Data arguments
opt.patchsize = 15
opt.nrofpatches = np.array([700000, 40000, np.inf])
opt.proportion = 0.1
opt.softlabel = True
opt.test_val_ratio = 5
opt.slidingwindowstride = 4

#Output arguments
opt.print_freq = 50000
opt.save_image_freq = 50000
opt.save_test_images = False
opt.mailadress = ''

# LOAD DATA
dataloader, opt = load_data(opt)

##
# LOAD MODEL
model = Ganomaly(opt, dataloader)

# TRAIN MODEL
#model.train_and_test()
#dataloader = []
#model = []
